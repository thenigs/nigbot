package com.bignigginit;

public class Entity {

	private int EntityId;
	private String entityName;
	private double xPos;
	private double yPos;
	private double zPos;
	
	public double serverx;
	public double servery;
	public double serverz;
	
	
	
	public Entity(int id, String name, double x, double y, double z){
		this.EntityId = id;
		this.entityName = name;
		this.setxPos(x);
		this.setyPos(y);
		this.setzPos(z);
	}

	public int getId() {
		// TODO Auto-generated method stub
		return this.EntityId;
	}
	
	public double getDistance(double par1, double par3, double par5)
    {
        double d3 = this.xPos - par1;
        double d4 = this.yPos - par3;
        double d5 = this.zPos - par5;
        return (double)MathHelper.sqrt_double(d3 * d3 + d4 * d4 + d5 * d5);
    }
	

	public String getEntityName() {
		return entityName;
	}

	

	public double getyPos() {
		return yPos;
	}

	public void setyPos(double yPos) {
		this.yPos = yPos;
	}

	public double getxPos() {
		return xPos;
	}

	public void setxPos(double xPos) {
		this.xPos = xPos;
	}

	public double getzPos() {
		return zPos;
	}

	public void setzPos(double zPos) {
		this.zPos = zPos;
	}
}
