package com.bignigginit.auth;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * User: Stl
 * Date: 4/9/2014
 * Time: 2:10 PM
 * Use:  Login agent for Yggdrasil authentification
 */
public class LoginAgent {

    @Expose
    @SerializedName("name")
    private String agentName;
    @Expose
    @SerializedName("version")
    private int agentVersion;

    public LoginAgent() {
        this("Minecraft", 1);
    }

    public LoginAgent(String agentName, int agentVersion) {
        this.agentName = agentName;
        this.agentVersion = agentVersion;
    }

    public String getAgentName() {
        return agentName;
    }

    public int getAgentVersion() {
        return agentVersion;
    }

}
