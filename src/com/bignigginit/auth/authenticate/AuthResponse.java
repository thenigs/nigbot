package com.bignigginit.auth.authenticate;

import com.bignigginit.auth.LoginProfile;
import com.bignigginit.auth.LoginResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * User: Stl
 * Date: 4/9/2014
 * Time: 4:00 PM
 * Use:
 */
public class AuthResponse implements LoginResponse {

    @Expose
    @SerializedName("accessToken")
    private String accessToken;
    @Expose
    @SerializedName("clientToken")
    private String clientToken;
    @Expose
    @SerializedName("availableProfiles")
    private List<LoginProfile> availableProfiles;
    @Expose
    @SerializedName("selectedProfile")
    private LoginProfile selectedProfile;
    @Expose
    @SerializedName("errorMessage")
    private String errorMessage;


    public String getAccessToken() {
        return accessToken;
    }

    public String getClientToken() {
        return clientToken;
    }

    public List<LoginProfile> getAvailableProfiles() {
        return availableProfiles;
    }

    public LoginProfile getSelectedProfile() {
        return selectedProfile;
    }
    
    public String getError(){
    	return this.errorMessage;
    }

}
