package com.bignigginit.auth.authenticate;

import java.net.Proxy;

import com.bignigginit.Session;
import com.bignigginit.auth.LoginPayload;
import com.bignigginit.auth.YggdrasilAuthenticator;


/**
 * User: Stl
 * Date: 2/2/14
 * Time: 4:48 PM
 * Use:  Thread to log in to Minecraft
 */
public class AuthThread implements Runnable {

    private final LoginPayload payload;
    private final YggdrasilAuthenticator authenticator;
    private boolean setmcs;
    
    private Proxy proxy;
    
    public String username;
    
    public AuthThread(String username, String password, Proxy p) {
        this.payload = new AuthPayload(username, password);
        this.authenticator = new YggdrasilAuthenticator();
        this.proxy = p;
        this.username = username;
    }
    
    private Session session;

    @Override
    public void run() {
    	
    	
        AuthResponse response = this.authenticator.getResponse("authenticate", this.payload, AuthResponse.class, proxy);

        if(response.getError() != null){
        	System.out.println(this.username + ": " + response.getError());
        	
        }else{
        	this.session = new Session(response.getSelectedProfile().getName(),  response.getAccessToken());

        }
        
        
        
        
    }
    
    
    
    public Session getSession(){
    	return this.session;
    }

}
