package com.bignigginit.auth.authenticate;

import com.bignigginit.auth.LoginAgent;
import com.bignigginit.auth.LoginPayload;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * User: Stl
 * Date: 4/9/2014
 * Time: 2:10 PM
 * Use:
 */
public class AuthPayload implements LoginPayload {

    @Expose
    @SerializedName("agent")
    private LoginAgent agent;

    @Expose
    @SerializedName("username")
    private String username;
    @Expose
    @SerializedName("password")
    private String password;
    @Expose (serialize = false, deserialize = false)
    @SerializedName("clientToken")
    private String clientToken;

    public AuthPayload(String username, String password) {
        this(username, password, "", new LoginAgent());
    }

    public AuthPayload(String username, String password, LoginAgent agent) {
        this(username, password, "", agent);
    }

    public AuthPayload(String username, String password, String clientToken) {
        this(username, password, clientToken, new LoginAgent());
    }

    public AuthPayload(String username, String password, String clientToken, LoginAgent agent) {
        this.agent = agent;
        this.username = username;
        this.password = password;
        this.clientToken = clientToken;
    }

}
