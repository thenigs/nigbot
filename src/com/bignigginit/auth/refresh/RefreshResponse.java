package com.bignigginit.auth.refresh;

import com.bignigginit.auth.LoginProfile;
import com.bignigginit.auth.LoginResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * User: Stl
 * Date: 4/9/2014
 * Time: 10:44 PM
 * Use:
 */
public class RefreshResponse implements LoginResponse {

    @Expose
    @SerializedName("accessToken")
    private String accessToken;
    @Expose
    @SerializedName("clientToken")
    private String clientToken;

    @Expose
    @SerializedName("selectedProfile")
    private LoginProfile selectedProfile;

    public String getAccessToken() {
        return accessToken;
    }

    public String getClientToken() {
        return clientToken;
    }

    public LoginProfile getSelectedProfile() {
        return selectedProfile;
    }

}
