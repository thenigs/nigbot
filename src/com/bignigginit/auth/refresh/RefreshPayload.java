package com.bignigginit.auth.refresh;

import com.bignigginit.auth.LoginPayload;
import com.bignigginit.auth.LoginProfile;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * User: Stl
 * Date: 4/9/2014
 * Time: 4:54 PM
 * Use:
 */
public class RefreshPayload implements LoginPayload {

    @Expose
    @SerializedName("accessToken")
    private String accessToken;
    @Expose
    @SerializedName("clientToken")
    private String clientToken;

    @Expose
    @SerializedName("selectedProfile")
    private LoginProfile selectedProfile;

    public RefreshPayload(String accessToken, String clientToken, LoginProfile selectedProfile) {
        this.accessToken = accessToken;
        this.clientToken = clientToken;
        this.selectedProfile = selectedProfile;
    }

}