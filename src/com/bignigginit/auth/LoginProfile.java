package com.bignigginit.auth;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * User: Stl
 * Date: 4/9/2014
 * Time: 4:04 PM
 * Use:
 */
public class LoginProfile {

    @Expose
    @SerializedName("id")
    private String id;
    @Expose
    @SerializedName("name")
    private String name;
    @Expose
    @SerializedName("legacy")
    private boolean legacy;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isLegacy() {
        return legacy;
    }

}
