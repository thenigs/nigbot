package com.bignigginit.auth;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.net.ssl.HttpsURLConnection;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Proxy;
import java.net.URL;
import java.util.Scanner;

/**
 * User: Stl
 * Date: 4/9/2014
 * Time: 10:47 PM
 * Use:
 */
public class YggdrasilAuthenticator {

    private Gson gson;

    public YggdrasilAuthenticator() {
        this.gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    }

    public <T extends LoginResponse> T getResponse(String endpoint, LoginPayload payload, Class<T> responseClass, Proxy p) {
      
    	HttpsURLConnection connection = null;
    	try {
            Proxy proxy = p;

            if (proxy == null) {
                proxy = Proxy.NO_PROXY;
            }

            URL authUrl = new URL("https://authserver.mojang.com/" + endpoint);
            String payloadStr = this.gson.toJson(payload);

             connection = (HttpsURLConnection) authUrl.openConnection(proxy);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Content-Length", "" + payloadStr.getBytes().length);
            connection.setRequestProperty("Content-Language", "en-US");
            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream());
            outputStream.writeBytes(payloadStr);
            outputStream.flush();
            outputStream.close();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuffer response = new StringBuffer();
            String var7;

            while ((var7 = reader.readLine()) != null) {
                response.append(var7);
                response.append('\r');
            }

            reader.close();
            ;
            return gson.fromJson(response.toString(), responseClass);
        } catch (Exception exception) {
            try {
				if(connection.getResponseCode() == 403){
					Scanner s = new Scanner(connection.getErrorStream());
					
					
					return gson.fromJson(s.nextLine(), responseClass);
					
				}else{
					System.out.println("Unhandled error code: " + connection.getResponseCode());
					
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            return null;
        }
    }
}
