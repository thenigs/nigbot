package com.bignigginit;


public class DirectionUtil{

	public DirectionUtil(){

	}

	public enum Direction {

		NORTH,
		SOUTH,
		EAST,
		WEST,
		NONE
	}

	public enum Plane{
		NORTHEAST,
		NORTHWEST,
		SOUTHEAST,
		SOUTHWEST
	}



	private Plane getPlane(double x, double z){

		boolean pX = x > 0;

		boolean pZ = z > 0;

		if(pX && pZ){
			return Plane.SOUTHEAST;
		}else if (!pX && !pZ) {
			return Plane.NORTHWEST;	
		}else if(pX && !pZ){
			return Plane.NORTHEAST;
		}else if (!pX && pZ) {
			return Plane.SOUTHWEST;
		}

		return null;

	}


	class Modifier{

		double xmod;
		double zmod;

		public Modifier(Plane p){
			switch (p) {

			case NORTHWEST:

				xmod = -.5;
				zmod = -.5;

				break;
			case NORTHEAST:

				xmod = .5;
				zmod = -.5;

				break;
			case SOUTHWEST:

				xmod = -.5;
				zmod = .5;

				break;
			case SOUTHEAST:

				xmod = .5;
				zmod = .5;

				break;

			default:
				break;
			}
		}

	}




	public Direction getCollidedDirection(MinecraftBot bot){

		double xPos = bot.getNetHandler().player.getX();
		double yPos = bot.getNetHandler().player.getY();
		double zPos = bot.getNetHandler().player.getZ();

		Block northBlock = bot.getNetHandler().world.getBlockAt(xPos, yPos, zPos - 1);
		Block southBlock = bot.getNetHandler().world.getBlockAt(xPos, yPos, zPos + 1);
		Block eastBlock = bot.getNetHandler().world.getBlockAt(xPos + 1, yPos, zPos);
		Block westBlock = bot.getNetHandler().world.getBlockAt(xPos - 1, yPos, zPos);

		double cz = zPos % 1;

		double cx = xPos %1;

		boolean inversez = false;

		boolean inversex = false;



		if(cz < 0){
			cz = -zPos % 1;
			inversez = true;
		}

		if(cx < 0){
			cx = -xPos % 1;
			inversex = true;
		}


		collidednorth  = (inversez?cz > .6 :cz < .4 ) ;

		collidedsouth = (inversez?cz < .4 :cz > .6 ) ;



		collidedwest =  (inversex?cx > .6 :cx < .4 ) ;

		collidedeast = (inversex?cx < .4 :cx > .6 ) ;




		if(collidednorth && !canBlockBeWalkedThrough(northBlock.getId())){
			return Direction.NORTH;
		}else if(collidedsouth && !canBlockBeWalkedThrough(southBlock.getId())){
			return Direction.SOUTH;
		}else if(collidedwest && !canBlockBeWalkedThrough(westBlock.getId())){
			return Direction.WEST;
		}else if(collidedeast && !canBlockBeWalkedThrough(eastBlock.getId())){
			return Direction.EAST;
		}

		return Direction.NONE;
	}

	public boolean canBlockBeWalkedThrough(int id) {
		return (id == 0 || id == 6 || id == 50 || id == 63 || id == 30 || id == 31 || id == 32 || id == 37 || id == 38 || id == 39 || id == 40 || id == 55 || id == 66 || id == 75
				|| id == 76 || id == 78 || id == 408 || id ==  328 || id ==  33 || id ==  342 || id ==  343 || id ==  407 || id ==  66 || id ==  27 || id ==  28 || id ==  157 || id ==  111
				|| id ==  390 || id ==  70 || id ==  72 || id ==  148 || id ==  147 || id ==  107 || id ==  69);
	}

	boolean collidednorth;
	boolean collidedsouth;
	boolean collidedwest; 
	boolean collidedeast; 
}
