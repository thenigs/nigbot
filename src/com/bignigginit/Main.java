package com.bignigginit;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import joptsimple.ArgumentAcceptingOptionSpec;
import joptsimple.NonOptionArgumentSpec;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpecBuilder;

import com.bignigginit.cmd.Command;
import com.bignigginit.cmd.SayCommand;


public class Main {
	
	public static List<MinecraftBot> bots= new ArrayList<MinecraftBot>();
	
	static String fullIp;
	
	static String ip = "localhost";
	
	static int port  = 25565;
	public static int maxUsers = 9999;
	
	public static boolean movement;
	
	public static void main(String[] args){
			
		Scanner scanner = new Scanner(System.in);
		
		OptionParser optionparser = new OptionParser();
		
		ArgumentAcceptingOptionSpec<String> iparg = optionparser.accepts("ip").withRequiredArg();
		
		ArgumentAcceptingOptionSpec<String> max = optionparser.accepts("max").withRequiredArg();
		
		OptionSpecBuilder movementoption = optionparser.accepts("move");
		
		NonOptionArgumentSpec nonOptions = optionparser.nonOptions();
        OptionSet optionset = optionparser.parse(args);
        
        try{
        	maxUsers = Integer.parseInt(optionset.valueOf(max));
        }catch(Exception e){
        	
        }
        
        
       
        if(optionset.has("move")){
        	movement = true;
        }
        
        if(!optionset.has("ip")){
        	
        	System.out.println("Enter server ip:port");
			
    		fullIp = scanner.nextLine();
        	
        }else{
        	
        	fullIp = optionset.valueOf(iparg);
        	
        }
        
        
        
		String[] splitIp = fullIp.split(":");
		
		ip = splitIp[0];
		
		if(fullIp.contains(":")){
			port = Integer.valueOf(splitIp[1]);
		}else{
			port = 25565;
		}
		
		bots= new ArrayList<MinecraftBot>();
		Read();
		
		
		if(bots.size() > 0){

		bots.get(0).parseChat = true;
		
		
			
			while(scanner.hasNext()){
				
				String message = scanner.nextLine();
				
				
				if(message.startsWith("get")){
	
					cmdGet(message);
	
				}else if (message.startsWith("all")){
					cmdAll(message);
					
				}else{
					System.out.println("Specificy 'all' or 'get [name]' then command");
				}
				
	
			}
		scanner.close();
		}else{
			System.out.println("No bots Loaded");
		}
	}
	
	private static void cmdGet(String message){
		MinecraftBot b = getBotByName(message.split(" ")[1]);
		
		if(b ==null){
			System.out.println("Bot not found");
		}else{
			message = message.toLowerCase().replaceFirst("get " + b.getUsername().toLowerCase() + " ", "");
			
			Command command = Command.getCommand(message.split(" ")[0]);
			
			if(command != null){
				String arguments = message.replaceFirst(command.getPrefix(), "");
				arguments = arguments.trim();
				
				String [] cmdargs = arguments.split(" ");
				
				command.construct(cmdargs, b);
				
				command.run();
				
			}else{
				System.out.println("Unknown Command");
			}
		}
	}
	
	private static void cmdAll(String message){
		message = message.replaceFirst("all ", "");
		
		for(MinecraftBot b: bots){
			
			Command command = Command.getCommand(message.split(" ")[0]);
			
			if(command != null){
				String arguments = message.replaceFirst(command.getPrefix(), "");
				arguments = arguments.trim();
				
				String [] cmdargs = arguments.split(" ");
				
				//System.out.println(arguments);
				command.construct(cmdargs, b);
				
				command.run();
				if(!(command instanceof SayCommand)){
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}else{
				System.out.println("Unknown Command");
			}
			
		}
		
		
	}
	
	private static MinecraftBot getBotByName(String string) {
		for(MinecraftBot bot : bots){
			if(bot.getUsername().equalsIgnoreCase(string))
				return bot;
		}
		return null;
	}

	static List<Proxy> proxies = new ArrayList<Proxy>();
	
	static List<Thread> threads = new ArrayList<Thread>();
	
	public static void Read(){
		File userFile = new File("users.txt");
		Scanner userScanner;
		
		
		
		
		try {
			userScanner = new Scanner(userFile);
			
		
			
			int counter = 0;
			
			

			
			while(userScanner.hasNextLine() && counter < maxUsers)
			{
				
				
				String[] f = userScanner.nextLine().split(":");
				
				final String usernameString = f[0];
				
				final String passwordString = f[1];
				
					
				
				threads.add(new Thread(){
					
					public void run(){
						
						MinecraftBot bot = new MinecraftBot(usernameString, passwordString, ip, port,Proxy.NO_PROXY);
						
						if(bot.session != null && bot != null){
							
							
							bots.add(bot);
							System.out.println("Loaded user: " + bot.getUsername());
							
								
						}
						
						
					}
					
					
				});
				
				
				counter++;			
				
						
						

			}
			
			
			for(Thread t: threads){
				t.start();
			}
			
			check:
				while(true){
					for(Thread t: threads){
						if(t.isAlive())
							continue check;
					}
					break check;
				}
			check2:
			while (true) {
				for(MinecraftBot bot : bots){
					if(bot == null){
						continue check2;
					}
				}
				break check2;
			}
				
				
				System.out.println("Loaded " + bots.size() + " users");
			
			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	

}
