package com.bignigginit.cmd;

import javax.xml.stream.events.StartDocument;

import com.bignigginit.Entity;

public class FollowCommand extends Command{

	protected FollowCommand(String p) {
		super(p);
	}

	
	
	@Override
	public void run() {

			Entity e = bot.getNetHandler().world.getEntityByName(args[0]);
			
			if(e != null){
				bot.getPlayer().following = e.getEntityName();
			}else{
				bot.getPlayer().following = "";
				System.out.println("No entity by that name found");
			}
			
			
			
			if(bot.getPlayer().following != "")
			bot.getPlayer().moveToEntity(e);
			

	}


}
