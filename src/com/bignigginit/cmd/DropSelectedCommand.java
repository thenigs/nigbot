package com.bignigginit.cmd;

public class DropSelectedCommand extends Command {

	protected DropSelectedCommand(String p) {
		super(p);
	}

	@Override
	public void run() {
		bot.getNetHandler().currentInventory.dropSelectedItem();
	}

}
