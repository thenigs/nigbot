package com.bignigginit.cmd;

import com.bignigginit.PlayerInventory;

public class DropFirstByIdCommand extends Command {

	protected DropFirstByIdCommand(String p) {
		super(p);
	}

	@Override
	public void run() {
		if(bot.getNetHandler().currentInventory instanceof PlayerInventory)
		bot.getNetHandler().playerInventory.dropFirstById(Integer.parseInt(args[0]));
		else{
			System.out.println("Must not be in any inventory");
		}
	}

}
