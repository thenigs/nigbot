package com.bignigginit.cmd;

public class ClickBlockCommand extends Command {

	protected ClickBlockCommand(String p) {
		super(p);
	}

	@Override
	public void run() {
		bot.getNetHandler().player.clickBlock(Integer.parseInt(args[0]),Integer.parseInt(args[1]), Integer.parseInt(args[2]));
	}

}
