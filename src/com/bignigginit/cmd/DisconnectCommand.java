package com.bignigginit.cmd;

public class DisconnectCommand extends Command {

	protected DisconnectCommand(String p) {
		super(p);
		
	}

	@Override
	public void run() {
		bot.getConnection().disconnect();
	}

	

}
