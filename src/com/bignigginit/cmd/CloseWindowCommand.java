package com.bignigginit.cmd;

public class CloseWindowCommand extends Command {

	protected CloseWindowCommand(String p) {
		super(p);
	}

	@Override
	public void run() {
		bot.getNetHandler().currentInventory.close();
	}

}
