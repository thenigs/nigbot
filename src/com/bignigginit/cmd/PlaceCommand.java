package com.bignigginit.cmd;

public class PlaceCommand extends Command {

	protected PlaceCommand(String p) {
		super(p);
	}

	@Override
	public void run() {
		bot.getPlayer().placeBlock(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]));
	}

}
