package com.bignigginit.cmd;

import com.bignigginit.ChestInventory;

public class TakeFirstCommand extends Command {

	protected TakeFirstCommand(String p) {
		super(p);
	}

	@Override
	public void run() {
		if(bot.getNetHandler().currentInventory instanceof ChestInventory){
			bot.getNetHandler().currentInventory.takeFirst(Integer.parseInt(args[0]));
		}else{
			System.out.println("Not in a chest");
		}
	}

}
