package com.bignigginit.cmd;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.ArrayStack;

import com.bignigginit.MinecraftBot;

public abstract class Command {
	
	protected String prefix;
	
	protected String[] args;
	
	protected MinecraftBot bot;
	
	protected Command(String p){
		this.prefix = p;
	}
	
	public abstract void run();
	

	public  String getPrefix(){
		return this.prefix;
	}
	
	private static List<Command> commands = Arrays.asList(new Command[]{
			new MoveCommand("move"),
			new FollowCommand("follow"),
			new SayCommand("say"),
			new ConnectCommand("connect"),
			new DisconnectCommand("disconnect"),
			new TpaCommand("tpa"),
			new SelectItemCommand("selectitem"),
			new PlaceCommand("place"),
			new ClickBlockCommand("clickblock"),
			new CloseWindowCommand("closewindow"),
			new DropSelectedCommand("dropselected"),
			new EquipCommand("equip"),
			new DropFirstByIdCommand("dropfirst"),
			new TakeFirstCommand("takefirst"),
			new DigCommand("dig")
			
			
	});
		
	
	public void construct(String[] arg, MinecraftBot b){
		this.args = arg;
		this.bot = b;
	}
	
	
	
	
	public static Command getCommand(String s){
		for(Command command : commands){
			if(command.getPrefix().equalsIgnoreCase(s))
				return command;
		}
		return null;
	}
	

}
