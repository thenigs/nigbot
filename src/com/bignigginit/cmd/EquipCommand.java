package com.bignigginit.cmd;

import com.bignigginit.PlayerInventory;

public class EquipCommand extends Command {

	protected EquipCommand(String p) {
		super(p);
	}

	@Override
	public void run() {
		
		if(bot.getNetHandler().currentInventory instanceof PlayerInventory)
		bot.getNetHandler().playerInventory.equipIdFromInventory(Integer.parseInt(args[0]));
		else
		System.out.println("Must not be in chest");
	}

}
