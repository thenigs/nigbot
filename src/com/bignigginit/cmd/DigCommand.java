package com.bignigginit.cmd;

import com.bignigginit.BlockLocation;

public class DigCommand extends Command {

	protected DigCommand(String p) {
		super(p);
	}

	@Override
	public void run() {
		bot.getPlayer().digBlock(new BlockLocation(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2])));
	}

}
