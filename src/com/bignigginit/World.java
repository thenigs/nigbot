package com.bignigginit;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;





public class World {

	private final Map<ChunkLocation, Chunk> chunks;
	
	private MinecraftBot bot;
	
	private String worldTypeString;
	
	private int dimension;
	
	private byte difficulty;
	
	private int height;
	
	 private IntHashMap entityHashSet = new IntHashMap();
	 
	 private Set<Entity> entityList = new HashSet<Entity>();

	
	
	public World(MinecraftBot bot, String type, int dimension, byte difficulty, int height){
		chunks = new HashMap<ChunkLocation, Chunk>();
		
		this.setWorldTypeString(type);
		this.setHeight(height);
		this.setDimension(dimension);
		this.setDifficulty(difficulty);
	}
	
	public void onChunkLoad(ChunkLoadEvent event) {
		ChunkLocation location = new ChunkLocation(event.getX(), event.getY(), event.getZ());
		Chunk chunk = new Chunk(this, location, event.getBlocks(), event.getMetadata(), event.getLight(), event.getSkylight(), event.getBiomes());
		synchronized(chunks) {
			chunks.put(location, chunk);
		}
		//bot.getEventBus().fire(new ChunkLoadEvent(this, chunk));
	}
	
	public Entity getEntityByName(String string) {
		for(Entity e : entityList){
			if(e.getEntityName().equalsIgnoreCase(string)){
				return e;
			}
		}
		return null;
	}
	
	public void addEntityToWorld(Entity par2Entity)
    {
        Entity entity1 = this.getEntityByID(par2Entity.getId());

        if (entity1 != null)
        {
            this.removeEntity(entity1);
        }

        this.entityList.add(par2Entity);


        this.entityHashSet.addKey(par2Entity.getId(), par2Entity);
    }
	
	public void removeEntity(Entity par1Entity)
    {
        this.entityList.remove(par1Entity);
    }
	
	public Entity getEntityByID(int par1)
    {
        return  (Entity)this.entityHashSet.lookup(par1);
    }

	public Block getBlockAt(double x, double y, double z) {
		return getBlockAt(new BlockLocation(MathHelper.floor_double(x), MathHelper.floor_double(y), MathHelper.floor_double(z)));
	}
	
	public Block getBlockAt(int x, int y, int z) {
		return getBlockAt(new BlockLocation(x, y, z));
	}

	
	
	
	public Block getBlockAt(BlockLocation location) {
		ChunkLocation chunkLocation = new ChunkLocation(location);
		Chunk chunk = getChunkAt(chunkLocation);
		if(chunk == null)
			return null;
		BlockLocation chunkBlockOffset = new BlockLocation(chunkLocation);
		int chunkOffsetX = location.getX() - chunkBlockOffset.getX();
		int chunkOffsetY = location.getY() - chunkBlockOffset.getY();
		int chunkOffsetZ = location.getZ() - chunkBlockOffset.getZ();
		int id = chunk.getBlockIdAt(chunkOffsetX, chunkOffsetY, chunkOffsetZ);
		int metadata = chunk.getBlockMetadataAt(chunkOffsetX, chunkOffsetY, chunkOffsetZ);
		return new Block(this, chunk, location, id, metadata);
	}

	
	public int getBlockIdAt(int x, int y, int z) {
		return getBlockIdAt(new BlockLocation(x, y, z));
	}

	
	public int getBlockIdAt(BlockLocation blockLocation) {
		ChunkLocation location = new ChunkLocation(blockLocation);
		BlockLocation chunkBlockOffset = new BlockLocation(location);
		Chunk chunk = getChunkAt(location);
		if(chunk == null)
			return 0;
		int id = chunk.getBlockIdAt(blockLocation.getX() - chunkBlockOffset.getX(), blockLocation.getY() - chunkBlockOffset.getY(), blockLocation.getZ()
				- chunkBlockOffset.getZ());
		return id;
	}
	
	public Chunk getChunkAt(ChunkLocation location) {
		synchronized(chunks) {
			return chunks.get(location);
		}
	}
	
	public void setBlockIdAt(int id, int x, int y, int z) {
		setBlockIdAt(id, new BlockLocation(x, y, z));
	}

	public void setBlockIdAt(int id, BlockLocation blockLocation) {
		ChunkLocation location = new ChunkLocation(blockLocation);
		BlockLocation chunkBlockOffset = new BlockLocation(location);
		Chunk chunk = getChunkAt(location);
		if(chunk == null)
			return;
		chunk.setBlockIdAt(id, blockLocation.getX() - chunkBlockOffset.getX(), blockLocation.getY() - chunkBlockOffset.getY(), blockLocation.getZ()
				- chunkBlockOffset.getZ());
	}
	
	public BlockLocation getCenter(double x, double y, double z){
        double rx;
        double rz;
        rx = Math.floor(x)+0.5;
        rz = Math.floor(z)+0.5;
        System.out.println("Center is: " + rx + " " + rz);
        return new BlockLocation((int)rx,(int)y,(int)rz);
    }

	
	public String getWorldTypeString() {
		return worldTypeString;
	}

	public void setWorldTypeString(String worldTypeString) {
		this.worldTypeString = worldTypeString;
	}

	public byte getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(byte difficulty) {
		this.difficulty = difficulty;
	}

	public int getDimension() {
		return dimension;
	}

	public void setDimension(int dimension) {
		this.dimension = dimension;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	
	
}
