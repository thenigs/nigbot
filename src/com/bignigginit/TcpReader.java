package com.bignigginit;

import java.io.DataInputStream;
import java.io.InputStream;

import com.bignigginit.packets.Packet106Transaction;
import com.bignigginit.packets.Packet252SharedKey;

public class TcpReader implements Runnable{

	MinecraftBot bot;
	
	public DataInputStream inputStream;
	
	private NetHandler theNetHandler;
	
	TcpConnection connection;
	
	PacketHandler packetHandler;
	
	boolean isInputDecrypted;

	TcpReader(MinecraftBot m, DataInputStream in, NetHandler handler, TcpConnection connection, PacketHandler p){
		
		this.bot = m;
		this.inputStream = in;
		this.connection = connection;
		this.theNetHandler = handler;
		this.packetHandler = p;
		isReading = true;
		this.connection.disconnect = false;
	}
	
	
	@Override
	public void run() {
		
		while(bot.isRunning && this.isReading){
			if(this.connection.disconnect){
				break;
			}
			
			readPacket(inputStream);
			
			
		}
		
	}
	
	
	
	 boolean isReading;
	 
	 public void setReading(boolean r){
			 this.isReading = r;
	 }
	
	 
	 
	
	 boolean readPacket(DataInputStream in)
    {

        boolean flag = false;
        
        try
        {
        	
        	
            Packet packet = packetHandler.readPacket(in, this);
            
            

            if (packet != null)
            {
            	
                 packet.readPacketData(in);
                 
            
                //System.out.println("READ: " + packet.getPacketId());
            	
                if (packet instanceof Packet252SharedKey && !isInputDecrypted)
                {
                	isInputDecrypted = true;
                	InputStream cin = CryptManager.decryptInputStream(connection.sharedKeyForEncryption, this.inputStream);
                	inputStream = new DataInputStream(cin);
                	System.out.println("Logging in " + this.bot.getUsername());
                	this.connection.isLoggedIn = true;
                	
                	
                }
                
                if(packet instanceof Packet106Transaction){
                	bot.getNetHandler().currentInventory.onPacket106();
                }

                
                packet.processPacket(this.theNetHandler);
                
                flag = true;

            }


            return flag;
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            connection.getBot().isRunning = false;
            return false;
        }
        
        
        
        
    }
	
	
	
}