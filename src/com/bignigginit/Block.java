package com.bignigginit;


public class Block {

	private final World world;
	private final Chunk chunk;
	private final BlockLocation location;
	private final int id, metadata;

	public Block(World world, Chunk chunk, BlockLocation location, int id,
			int metadata) {
		this.world = world;
		this.chunk = chunk;
		this.location = location;
		this.id = id;
		this.metadata = metadata;
	}

	public Block getRelative(int bx, int by, int bz){
        System.out.println("Getting the block relative to: " + location.getX() + " ," + location.getY() + " ," + location.getZ() + " looking at: " + (bx+location.getX()) + " , " + (location.getY()+by) +  " ," + (location.getZ()+bz));
        Block b = world.getBlockAt(location.getX()+bx, location.getY()+by, location.getZ()+bz);
        if(b == null){
            System.out.println("B is null, ending the world....");
            
        }
        return b;
    }
	
	public World getWorld() {
		return world;
	}

	public Chunk getChunk() {
		return chunk;
	}

	public BlockLocation getLocation() {
		return location;
	}

	public int getId() {
		return id;
	}

	public int getMetadata() {
		return metadata;
	}
}
