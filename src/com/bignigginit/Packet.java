package com.bignigginit;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.bignigginit.nbt.NBTTagCompound;

public abstract class Packet {
	
	
	
	

	public abstract void readPacketData(DataInput in) throws IOException;

    /**
     * Abstract. Writes the raw packet data to the data stream.
     */
    public abstract void writePacketData(DataOutput out) throws IOException;

    /**
     * Passes this Packet on to the NetHandler for processing.
     * @param theNetHandler 
     */
    public abstract void processPacket(NetHandler theNetHandler);
    
    public static void writeByteArray(DataOutput par0DataOutput, byte[] par1ArrayOfByte) throws IOException
    {
        par0DataOutput.writeShort(par1ArrayOfByte.length);
        par0DataOutput.write(par1ArrayOfByte);
    }

    /**
     * the first short in the stream indicates the number of bytes to read
     */
    public static byte[] readBytesFromStream(DataInput par0DataInput) throws IOException
    {
        short short1 = par0DataInput.readShort();

        if (short1 < 0)
        {
            throw new IOException("Key was smaller than nothing!  Weird key!");
        }
        else
        {
            byte[] abyte = new byte[short1];
            par0DataInput.readFully(abyte);
            return abyte;
        }
    }
    
    public static void writePacket(Packet par0Packet, DataOutputStream par1DataOutput) throws IOException
    {
        par1DataOutput.write(par0Packet.getPacketId());
        par0Packet.writePacketData(par1DataOutput);

    }

    
	public abstract int getPacketId();
	
	/**
     * Writes a String to the DataOutputStream
     */
    public static void writeString(String par0Str, DataOutput par1DataOutput) throws IOException
    {
        if (par0Str.length() > 32767)
        {
            throw new IOException("String too big");
        }
        else
        {
            par1DataOutput.writeShort(par0Str.length());
            par1DataOutput.writeChars(par0Str);
        }
    }

    /**
     * Reads a string from a packet
     */
    public static String readString(DataInput par0DataInput, int par1) throws IOException
    {
        short short1 = par0DataInput.readShort();

        if (short1 > par1)
        {
            throw new IOException("Received string length longer than maximum allowed (" + short1 + " > " + par1 + ")");
        }
        else if (short1 < 0)
        {
            throw new IOException("Received string length is less than zero! Weird string!");
        }
        else
        {
            StringBuilder stringbuilder = new StringBuilder();

            for (int j = 0; j < short1; ++j)
            {
                stringbuilder.append(par0DataInput.readChar());
            }

            return stringbuilder.toString();
        }
    }
    
    public static ItemStack readItemStack(DataInput in) throws IOException {
		ItemStack item = null;
		short id = in.readShort();

		if(id >= 0) {
			byte stackSize = in.readByte();
			short damage = in.readShort();
			item = new ItemStack(id, stackSize, damage);
			item.setStackTagCompound(readNBTTagCompound(in));
		}

		return item;
	}
    
    public static NBTTagCompound readNBTTagCompound(DataInput in) throws IOException {
		short length = in.readShort();

		if(length >= 0) {
			byte[] data = new byte[length];
			in.readFully(data);
			return CompressedStreamTools.decompress(data);
		} else
			return null;
	}
    
    public static List readWatchableObjects(DataInput par0DataInput) throws IOException
    {
        ArrayList arraylist = null;

        for (byte b0 = par0DataInput.readByte(); b0 != 127; b0 = par0DataInput.readByte())
        {
            if (arraylist == null)
            {
                arraylist = new ArrayList();
            }

            int i = (b0 & 224) >> 5;
            int j = b0 & 31;
            WatchableObject watchableobject = null;

            switch (i)
            {
                case 0:
                    watchableobject = new WatchableObject(i, j, Byte.valueOf(par0DataInput.readByte()));
                    break;
                case 1:
                    watchableobject = new WatchableObject(i, j, Short.valueOf(par0DataInput.readShort()));
                    break;
                case 2:
                    watchableobject = new WatchableObject(i, j, Integer.valueOf(par0DataInput.readInt()));
                    break;
                case 3:
                    watchableobject = new WatchableObject(i, j, Float.valueOf(par0DataInput.readFloat()));
                    break;
                case 4:
                    watchableobject = new WatchableObject(i, j, Packet.readString(par0DataInput, 64));
                    break;
                case 5:
                    watchableobject = new WatchableObject(i, j, Packet.readItemStack((DataInputStream) par0DataInput));
                    break;
                case 6:
                    int k = par0DataInput.readInt();
                    int l = par0DataInput.readInt();
                    int i1 = par0DataInput.readInt();
                   // watchableobject = new WatchableObject(i, j, new ChunkCoordinates(k, l, i1));
            }

            arraylist.add(watchableobject);
        }

        return arraylist;
    }
    
    public static void writeItemStack(ItemStack item, DataOutput out) throws IOException {
		if(item != null) {
			out.writeShort(item.getId());
			out.writeByte(item.getStackSize());
			out.writeShort(item.getDamage());

			writeNBTTagCompound(item.getStackTagCompound(), out);
		} else
			out.writeShort(-1);
	}
    
    public static void writeNBTTagCompound(NBTTagCompound compound, DataOutput out) throws IOException {
		if(compound != null) {
			byte[] data = CompressedStreamTools.compress(compound);
			out.writeShort((short) data.length);
			out.write(data);
		} else
			out.writeShort(-1);
	}


}
