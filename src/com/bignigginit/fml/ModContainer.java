package com.bignigginit.fml;

import java.io.File;
import java.security.cert.Certificate;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.ImmutableMap;
import com.google.common.eventbus.EventBus;



public interface ModContainer
{
    /**
     * The globally unique modid for this mod
     */
    String getModId();

    /**
     * A human readable name
     */

    String getName();

    /**
     * A human readable version identifier
     */
    String getVersion();

    /**
     * The location on the file system which this mod came from
     */
    File getSource();

    /**
     * The metadata for this mod
     */


    /**
     * Attach this mod to it's metadata from the supplied metadata collection
     */


    /**
     * Set the enabled/disabled state of this mod
     */
    void setEnabledState(boolean enabled);

    /**
     * A list of the modids that this mod requires loaded prior to loading
     */


    /**
     * A list of modids that should be loaded prior to this one. The special
     * value <strong>*</strong> indicates to load <em>after</em> any other mod.
     */


    /**
     * A list of modids that should be loaded <em>after</em> this one. The
     * special value <strong>*</strong> indicates to load <em>before</em> any
     * other mod.
     */
    /**
     * A representative string encapsulating the sorting preferences for this
     * mod
     */
    String getSortingRules();

    /**
     * Register the event bus for the mod and the controller for error handling
     * Returns if this bus was successfully registered - disabled mods and other
     * mods that don't need real events should return false and avoid further
     * processing
     *
     * @param bus
     * @param controller
     */
    /**
     * Does this mod match the supplied mod
     *
     * @param mod
     */
    boolean matches(Object mod);

    /**
     * Get the actual mod object
     */
    Object getMod();



    boolean isImmutable();

    boolean isNetworkMod();

    String getDisplayVersion();



    Certificate getSigningCertificate();

    public static final Map<String,String> EMPTY_PROPERTIES = ImmutableMap.of();
    Map<String,String> getCustomModProperties();

    public Class<?> getCustomResourcePackClass();

    Map<String, String> getSharedModDescriptor();
}