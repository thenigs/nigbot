/*
 * Forge Mod Loader
 * Copyright (c) 2012-2013 cpw.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 *
 * Contributors:
 *     cpw - implementation
 */

package com.bignigginit.fml;


import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketAddress;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;







import com.bignigginit.NetHandler;
import com.bignigginit.TcpConnection;
import com.bignigginit.packets.Packet1Login;
import com.bignigginit.packets.Packet250CustomPayload;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.hash.Hashing;

import static com.bignigginit.fml.FMLPacket.Type.MOD_LIST_REQUEST;
public class FMLNetworkHandler
{
    private static final int FML_HASH = Hashing.murmur3_32().hashString("FML").asInt();
    private static final int PROTOCOL_VERSION = 0x2;
    private static final FMLNetworkHandler INSTANCE = new FMLNetworkHandler();

    // List of states for connections from clients to server
    static final int LOGIN_RECEIVED = 1;
    static final int CONNECTION_VALID = 2;
    static final int FML_OUT_OF_DATE = -1;
    static final int MISSING_MODS_OR_VERSIONS = -2;

    //private Map<NetLoginHandler, Integer> loginStates = Maps.newHashMap();
    private Map<ModContainer, NetworkModHandler> networkModHandlers = Maps.newHashMap();

    private Map<Integer, NetworkModHandler> networkIdLookup = Maps.newHashMap();

    public static void handlePacket250Packet(Packet250CustomPayload packet, TcpConnection network, NetHandler handler)
    {
        String target = packet.channel;

        if (target.startsWith("MC|"))
        {
            handler.handleVanilla250Packet(packet);
        }
        if (target.equals("FML"))
        {
            instance().handleFMLPacket(packet, network, handler);
        }
        
    }

    public static void onConnectionEstablishedToServer(NetHandler clientHandler, TcpConnection manager, Packet1Login login)
    {
       // NetworkRegistry.instance().clientLoggedIn(clientHandler, manager, login);
    }

    private void handleFMLPacket(Packet250CustomPayload packet, TcpConnection network, NetHandler netHandler)
    {
        FMLPacket pkt = FMLPacket.readPacket(network, packet.data);
        // Part of an incomplete multipart packet
        if (pkt == null)
        {
            return;
        }
        String userName = "";
        
            
          userName = network.session.getUsername();
            
        

        pkt.execute(network, this, netHandler, userName);
    }

   

    public static FMLNetworkHandler instance()
    {
        return INSTANCE;
    }

    public static Packet1Login getFMLFakeLoginPacket()
    {
        // Always reset compat to zero before sending our fake packet
        FMLNetworkHandler.setClientCompatibilityLevel((byte) 0);
        Packet1Login fake = new Packet1Login();
        // Hash FML using a simple function
        fake.clientEntityId = FML_HASH;
        // The FML protocol version
        fake.dimension = PROTOCOL_VERSION;
        fake.gameType = -1;
        fake.terrainType = "default";
        return fake;
    }

    public Packet250CustomPayload getModListRequestPacket()
    {
        return PacketDispatcher.getPacket("FML", FMLPacket.makePacket(MOD_LIST_REQUEST));
    }

    public void registerNetworkMod(NetworkModHandler handler)
    {
        networkModHandlers.put(handler.getContainer(), handler);
        networkIdLookup.put(handler.getNetworkId(), handler);
    }
    
    

    public Set<ModContainer> getNetworkModList()
    {
        return networkModHandlers.keySet();
    }

    

    public Map<Integer, NetworkModHandler> getNetworkIdMap()
    {
        return networkIdLookup;
    }

    

   

    public static InetAddress computeLocalHost() throws IOException
    {
        InetAddress add = null;
        List<InetAddress> addresses = Lists.newArrayList();
        InetAddress localHost = InetAddress.getLocalHost();
        for (NetworkInterface ni : Collections.list(NetworkInterface.getNetworkInterfaces()))
        {
            if (!ni.isLoopback() && ni.isUp())
            {
                addresses.addAll(Collections.list(ni.getInetAddresses()));
                if (addresses.contains(localHost))
                {
                    add = localHost;
                    break;
                }
            }
        }
        if (add == null && !addresses.isEmpty())
        {
            for (InetAddress addr: addresses)
            {
                if (addr.getAddress().length == 4)
                {
                    add = addr;
                    break;
                }
            }
        }
        if (add == null)
        {
            add = localHost;
        }
        return add;
    }


    public static int getCompatibilityLevel()
    {
        return PROTOCOL_VERSION;
    }

    static byte compatibility;
    
    public static boolean vanillaLoginPacketCompatibility()
    {
    	//System.out.println(getClientCompatibilityLevel());
        return getClientCompatibilityLevel() == 0;
    }
    
    private static byte getClientCompatibilityLevel() {
		// TODO Auto-generated method stub
		return compatibility;
	}

	public static void setClientCompatibilityLevel(byte compatibilityLevel) {
		compatibility = compatibilityLevel;
		
	}

	
   
}
