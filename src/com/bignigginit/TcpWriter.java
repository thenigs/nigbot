package com.bignigginit;

import java.io.DataOutputStream;
import java.io.OutputStream;




import oracle.jrockit.jfr.tools.ConCatRepository;

import com.bignigginit.packets.Packet252SharedKey;
import com.sun.corba.se.pept.transport.Connection;

public class TcpWriter implements Runnable {

	
	MinecraftBot bot;
	
	DataOutputStream outputStream;
	
	TcpConnection tcpConnection;
	
	private NetHandler theNetHandler;
	
	boolean isOutputCrypted;
	//private CopyOnWriteArrayList<Packet> sendque = new CopyOnWriteArrayList<Packet>();
	
	TcpWriter(MinecraftBot m, DataOutputStream os, NetHandler handler, TcpConnection connection){
	
		this.bot = m;
		this.outputStream = os;
		this.theNetHandler = handler;
		this.tcpConnection = connection;
		tcpConnection.disconnect = false;
	}
	boolean iswriting;
	public void addtoQue(Packet p){
		
		if(this.tcpConnection.disconnect)
			return;
		
		if(!iswriting){
			iswriting = true;

			try{
				
						Packet.writePacket(p, outputStream);
						

						if(p instanceof Packet252SharedKey && !isOutputCrypted){
							
							isOutputCrypted = true;
							
							tcpConnection.sharedKeyForEncryption = ((Packet252SharedKey)p).getSharedKey();
						
							OutputStream cout = CryptManager.encryptOuputStream(tcpConnection.sharedKeyForEncryption, this.outputStream);
				        	
				        	
				        	this.outputStream = new DataOutputStream(cout);
				        	
							
						}
						

			}catch(Exception e){
				//e.printStackTrace();
				//bot.isRunning = false;
			}
			iswriting = false;
		}else{
			//System.out.println("cloudnt write " + p.getPacketId());
		}
		
		
	}

	
	
	@Override
	public void run() {
		
		
		
	}
	
	
}