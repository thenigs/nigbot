package com.bignigginit;


public class InventoryChange {
	private final int slot;
	private final int button;
	private final short transactionId;
	private final ItemStack item;
	private final boolean shiftHeld;
	private final int windowId;

	public InventoryChange(int windowId, int slot, int button, short transactionId, ItemStack item, boolean shiftHeld) {
		this.windowId = windowId;
		this.slot = slot;
		this.button = button;
		this.transactionId = transactionId;
		this.item = item;
		this.shiftHeld = shiftHeld;
	}

	public int getSlot() {
		return slot;
	}

	public int getButton() {
		return button;
	}

	public short getTransactionId() {
		return transactionId;
	}

	public ItemStack getItem() {
		return item;
	}

	public boolean isShiftHeld() {
		return shiftHeld;
	}

	public int getWindowId() {
		return windowId;
	}
}