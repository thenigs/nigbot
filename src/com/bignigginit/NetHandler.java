package com.bignigginit;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.Proxy;
import java.net.URL;
import java.net.URLEncoder;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.crypto.SecretKey;

import org.apache.commons.collections.functors.WhileClosure;

import sun.reflect.generics.tree.BottomSignature;

import com.bignigginit.auth.LoginPayload;
import com.bignigginit.auth.LoginResponse;
import com.bignigginit.fml.FMLNetworkHandler;
import com.bignigginit.packets.Packet0KeepAlive;
import com.bignigginit.packets.Packet100OpenWindow;
import com.bignigginit.packets.Packet101CloseWindow;
import com.bignigginit.packets.Packet102WindowClick;
import com.bignigginit.packets.Packet103SetSlot;
import com.bignigginit.packets.Packet104WindowItems;
import com.bignigginit.packets.Packet106Transaction;
import com.bignigginit.packets.Packet10Flying;
import com.bignigginit.packets.Packet13PlayerLookMove;
import com.bignigginit.packets.Packet16BlockItemSwitch;
import com.bignigginit.packets.Packet1Login;
import com.bignigginit.packets.Packet201PlayerInfo;
import com.bignigginit.packets.Packet205ClientCommand;
import com.bignigginit.packets.Packet20NamedEntitySpawn;
import com.bignigginit.packets.Packet250CustomPayload;
import com.bignigginit.packets.Packet252SharedKey;
import com.bignigginit.packets.Packet253ServerAuthData;
import com.bignigginit.packets.Packet255Kick;
import com.bignigginit.packets.Packet30Entity;
import com.bignigginit.packets.Packet31RelEntityMove;
import com.bignigginit.packets.Packet33RelEntityMoveLook;
import com.bignigginit.packets.Packet34EntityTeleport;
import com.bignigginit.packets.Packet3Chat;
import com.bignigginit.packets.Packet4UpdateTime;
import com.bignigginit.packets.Packet51MapChunk;
import com.bignigginit.packets.Packet53BlockChange;
import com.bignigginit.packets.Packet55BlockDetroy;
import com.bignigginit.packets.Packet56MapChunks;
import com.bignigginit.packets.Packet6SpawnPosition;
import com.bignigginit.packets.Packet8UpdateHealth;
import com.bignigginit.packets.Packet9Respawn;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;






public class NetHandler {
	
	public TcpConnection connection;
	
	
	public World world;
	
	public Player player;
	
	public Inventory currentInventory;
	
	public PlayerInventory playerInventory;
	
	private Gson gson;
	
	public NetHandler(TcpConnection c){
		
		this.connection = c;
		this.gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		
	}
	
	public void addToQue(Packet p){
		
		this.connection.getWriter().addtoQue(p);
		
	}

	
	public void handleSharedKey(Packet252SharedKey packet252SharedKey) {
		this.addToQue(FMLNetworkHandler.getFMLFakeLoginPacket());
		this.addToQue(new Packet205ClientCommand(0));
		
		
		
	}
	


	public void handleServerAuthData(Packet253ServerAuthData packet253ServerAuthData) {
		
		String s = packet253ServerAuthData.getServerId().trim();
        PublicKey publickey = packet253ServerAuthData.getPublicKey();
        SecretKey secretkey = CryptManager.createNewSharedKey();

       if (!"-".equals(s))
        {
            String s1 = (new BigInteger(CryptManager.getServerIdHash(s, publickey, secretkey))).toString(16);
            String s2 = this.sendSessionRequest(
            		connection.session.getUsername(),
            		connection.session.getSessionID(),
            		s1);

            if (!"ok".equalsIgnoreCase(s2))
            {

                return;
            }
        }
       //System.out.println(connection.getBot().getUsername() + packet253ServerAuthData.getVerifyToken());
       
       
        this.addToQue(new Packet252SharedKey(secretkey, publickey, packet253ServerAuthData.getVerifyToken()));
		
	}
	
	private String sendSessionRequest(String par1Str, String par2Str, String par3Str)
    {
        try
        {
            URL url = new URL("http://session.minecraft.net/game/joinserver.jsp?user=" + urlEncode(par1Str) + "&sessionId=" + urlEncode(par2Str) + "&serverId=" + urlEncode(par3Str));
            InputStream inputstream = url.openConnection().getInputStream();
            BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(inputstream));
            String s3 = bufferedreader.readLine();
            bufferedreader.close();
            return s3;
        }
        catch (IOException ioexception)
        {
            return ioexception.toString();
        }
    }
	
	private static String urlEncode(String par0Str) throws IOException
    {
        return URLEncoder.encode(par0Str, "UTF-8");
    }

	public void handleLogin(Packet1Login l) {
		
		this.world = new World(connection.getBot(), l.terrainType, l.dimension, l.difficultySetting, l.worldHeight);
		
		
	}
	
	public void handleKeepAlive(Packet0KeepAlive par1Packet0KeepAlive)
    {
        this.addToQue(new Packet0KeepAlive(par1Packet0KeepAlive.randomId));
    }
	
	public void handleCustomPayload(Packet250CustomPayload packet250CustomPayload) {
	
		FMLNetworkHandler.handlePacket250Packet(packet250CustomPayload, connection, this);
		
	}

	public void handleBlockItemSwitch(
			Packet16BlockItemSwitch packet16BlockItemSwitch) {
		// TODO Auto-generated method stub
		
	}

	public void handleUpdateTime(Packet4UpdateTime packet4UpdateTime) {
		// TODO Auto-generated method stub
		
	}
	public boolean tpaccept;
	
	public boolean readyfortpa;
	
	public void handleChat(Packet3Chat packet3Chat) {
		if(connection.getBot().isParsingChat()){
			
			String message = packet3Chat.message;
			Message m = getResponse(Message.class, message);
			
			message = m.getMessage();
			
			if(message == null)
				message = packet3Chat.message;
			
			System.out.println("CHAT:" + message);
		}
		
		if(tpaccept){

			if(packet3Chat.message.toLowerCase().contains("has requested")){
				
				addToQue(new Packet3Chat("/tpaccept"));
				
			}
			
			if(packet3Chat.message.toLowerCase().contains("has teleported")){
				readyfortpa = true;
			}
		}
	}

	public <T extends Message> T getResponse(Class<T> responseClass, String s) {
		return gson.fromJson(s, responseClass);
	      
	}
	
	public void handlePlayerInfo(Packet201PlayerInfo packet201PlayerInfo) {
		// TODO Auto-generated method stub
		
	}

	

	public void handleFlying(Packet10Flying packet10Flying) {
		
	}

	boolean first = true;;
	
	int offset;
	
	
	int lastmsmove = (int) (System.currentTimeMillis() / 1000);


	
	
	public int timeSinceLastMove(){
		return (int) ((System.currentTimeMillis() / 1000) - lastmsmove);
	}
	
	public void handlePlayerLookMove(
			Packet13PlayerLookMove move) {
		
		
		if(Main.movement){

			if(first){
				player = new Player(connection.getBot(), move.x,move.stance, move.y, move.z, move.yaw, move.pitch, move.onGround);
				
				
				player.handleMovement();
            	
				
			}

			player.setX(move.x);
			player.setStance(move.stance);
			player.setY(move.y);
			player.setZ(move.z);
			player.setYaw(move.yaw);
			player.setPitch(move.pitch);
			player.setOnGround(move.onGround);
			
			
			
			if(!first && !player.movingforward && timeSinceLastMove() >= 1){
				lastmsmove = (int) (System.currentTimeMillis() / 1000);
				//int face = MathHelper.floor_double((double)(player.yaw * 4.0F / 360.0F) + 0.5D) & 3;
				//player.faceBehind(face);
				
				
				int offset = 0;
				
				Random generator = new Random();
				int val = 50 - generator.nextInt(101);
				
				
				offset += val ;
			

				player.moveForward(-player.getYaw() + offset);
				
				//if(player.getY() > move.y)
				player.yPos += .2;
				player.stance += .2;
				
				
				
				
			}else{
				first = false;
			}
			addToQue(move);
		}
		connection.startKeepAliveTick();
		
		
		
	}

	public void handleVanilla250Packet(Packet250CustomPayload packet) {
		// TODO Auto-generated method stub
		
	}

	public void handleKickDisconnect(Packet255Kick packet255Kick) {
		System.out.println(connection.getBot().getUsername() + " Kicked: " + packet255Kick.reason);
		connection.getBot().isRunning = false;
	}

	
	
	
	int lastms = (int) (System.currentTimeMillis() / 1000);


	
	
	public int timeSinceLastChunk(){
		return (int) ((System.currentTimeMillis() / 1000) - lastms);
	}
	

	
	public void handeMapChunk(Packet56MapChunks chunkPacket) {
		lastms = (int) (System.currentTimeMillis() / 1000);
		
		
		for(int i = 0; i < chunkPacket.primaryBitmap.length; i++)
			processChunk(chunkPacket.chunkX[i], chunkPacket.chunkZ[i], chunkPacket.chunkData[i], chunkPacket.primaryBitmap[i], chunkPacket.secondaryBitmap[i], chunkPacket.skylight, true);
		
		
		
	}
	
	
	
	private void processChunk(int x, int z, byte[] data, int bitmask, int additionalBitmask, boolean addSkylight, boolean addBiomes) {
		
		
		if(data == null)
			return;
		
		
		int chunksChanged = 0;
		for(int i = 0; i < 16; i++)
			if((bitmask & (1 << i)) != 0)
				chunksChanged++;
		if(chunksChanged == 0)
			return;
		int[] yValues = new int[chunksChanged];
		byte[][] allBlocks = new byte[chunksChanged][], allMetadata = new byte[chunksChanged][], allLight = new byte[chunksChanged][], allSkylight = new byte[chunksChanged][];
		byte[] biomes = new byte[256];
		int i = 0;
		for(int y = 0; y < 16; y++) {
			if((bitmask & (1 << y)) == 0)
				continue;
			yValues[i] = y;
			int dataIndex = i * 4096;
			byte[] blocks = Arrays.copyOfRange(data, dataIndex, dataIndex + 4096);
			dataIndex += ((chunksChanged - i) * 4096) + (i * 2048);
			byte[] metadata = Arrays.copyOfRange(data, dataIndex, dataIndex + 2048);
			dataIndex += chunksChanged * 2048;
			byte[] light = Arrays.copyOfRange(data, dataIndex, dataIndex + 2048);
			dataIndex += chunksChanged * 2048;
			byte[] skylight = null;
			if(addSkylight)
				skylight = Arrays.copyOfRange(data, dataIndex, dataIndex + 2048);

			byte[] perBlockMetadata = new byte[4096];
			byte[] perBlockLight = new byte[4096];
			byte[] perBlockSkylight = new byte[4096];

			for(int j = 0; j < 2048; j++) {
				int k = j * 2;
				perBlockMetadata[k] = (byte) (metadata[j] & 0x0F);
				perBlockLight[k] = (byte) (light[j] & 0x0F);
				if(addSkylight)
					perBlockSkylight[k] = (byte) (skylight[j] & 0x0F);
				k++;
				perBlockMetadata[k] = (byte) (metadata[j] >> 4);
				perBlockLight[k] = (byte) (light[j] >> 4);
				if(addSkylight)
					perBlockSkylight[k] = (byte) (skylight[j] >> 4);
			}

			allBlocks[i] = blocks;
			allMetadata[i] = perBlockMetadata;
			allLight[i] = perBlockLight;
			allSkylight[i] = perBlockSkylight;
			i++;
		}
		System.arraycopy(data, data.length - 256, biomes, 0, 256);
		//EventBus eventBus = bot.getEventBus();
		for(i = 0; i < chunksChanged; i++) {
			ChunkLoadEvent event = new ChunkLoadEvent(x, yValues[i], z, allBlocks[i], allMetadata[i], allLight[i], allSkylight[i], biomes.clone());
			world.onChunkLoad(event);
			
			//eventBus.fire(event);
		}
		
		
		
	}

	

	public void handeBlockChange(Packet53BlockChange change) {
		//System.out.println(change.type);
		world.setBlockIdAt(change.type, change.xPosition, change.yPosition, change.zPosition);
	}

	public void handeUpdateHealth(Packet8UpdateHealth packet8UpdateHealth) {
		
		player.setHealth(packet8UpdateHealth.healthMP);
		
		
		
		if(player.getHealth() < 1){
			new Thread(){
				public void run(){
					try {
						Thread.sleep(2000);
						
						addToQue(new Packet205ClientCommand(1));
						
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			}.start();
			
		}
	}

	public void handleChunkLoad(Packet51MapChunk chunk) {
		
		processChunk(chunk.x, chunk.z, chunk.chunkData, chunk.bitmask, chunk.additionalBitmask,false, chunk.biomes);
		
	}

	public void handleRespawn(Packet9Respawn l) {
		

		
		
		if(this.world.getDimension() != l.dimension || this.world.getWorldTypeString() != l.terrainType){
			this.world = new World(connection.getBot(), l.terrainType, l.dimension, l.difficultySetting, l.worldHeight);
			
		}else{
			world.setDimension(l.dimension);
			world.setDifficulty(l.difficultySetting);
			world.setHeight(l.worldHeight);
			world.setWorldTypeString(l.terrainType);
		}
		
	}

	public int getBlockUnderPlayer() {
		return world.getBlockIdAt((int)MathHelper.floor_double(player.getX()),(int) MathHelper.floor_double(player.getY() - .5), (int)MathHelper.floor_double(player.getZ()));
	}

	public void handeEntitySpawn(Packet20NamedEntitySpawn p) {
		
		Entity e = new Entity(p.entityId, p.name, p.xPosition / 32, p.yPosition / 32, p.zPosition / 32);
		
		e.serverx = p.xPosition;
		e.servery = p.yPosition;
		e.serverz = p.zPosition;
		
		
		
		world.addEntityToWorld(e);
	}

	public void handleEntityMove(Packet31RelEntityMove p) {
		

	
	}

	public void handleEntityLookMove(Packet33RelEntityMoveLook p) {
		
	}

	public void handleEntityTeleport(
			Packet34EntityTeleport p) {
		
		Entity e = world.getEntityByID(p.entityId);
		if(e != null){
			e.serverx = p.xPosition;
			e.servery = p.yPosition;
			e.serverz = p.zPosition;
			
			e.setxPos((e.serverx ) / 32);
			e.setyPos((e.servery ) / 32);
			e.setzPos((e.serverz ) / 32);
			
			
		}
		
		
		
		
	}

	public void handleEntity(Packet30Entity p) {
		Entity e = world.getEntityByID(p.entityId);
		if(e != null){
			e.serverx += p.xPosition;
			e.servery += p.yPosition;
			e.serverz += p.zPosition;
			
			e.setxPos((e.serverx ) / 32);
			e.setyPos((e.servery ) / 32);
			e.setzPos((e.serverz ) / 32);
			

			
		}
	}

	public void handSpawn(Packet6SpawnPosition packet6SpawnPosition) {
	
		
	}
	
	public class Transaction{
		public Transaction(){
			
		}
	}
	

	public void writeInventoryChange(final InventoryChange i){
		
		
		Packet102WindowClick packet = new Packet102WindowClick();
		packet.windowId = i.getWindowId();
		packet.slot = i.getSlot();
		packet.button = i.getButton();
		packet.action = i.getTransactionId();
		packet.item = i.getItem();
		packet.shift = i.isShiftHeld();


		addToQue(packet);
		
		
	}

	public void handleTransaction(Packet106Transaction packet106Transaction) {
		
		
		if(!packet106Transaction.accepted){
			
			
			System.out.println("Transaction not accepted by server");
			
			Packet106Transaction p = new Packet106Transaction();
			p.windowId = packet106Transaction.windowId;
			p.shortWindowId = packet106Transaction.shortWindowId;
			p.accepted = true;
			addToQue(p);
		}

	}
	
	public void handleOpenWindow(Packet100OpenWindow packet100OpenWindow) {
		if(packet100OpenWindow.inventoryType.equals(InventoryType.CHEST)){
			currentInventory = new ChestInventory(connection.getBot(), packet100OpenWindow.windowId, packet100OpenWindow.slotsCount);
			player.inChest = true;
			System.out.println("Opening inventory " + packet100OpenWindow.windowTitle);
			
		}else{
			System.out.println("Unhandled open window: " + packet100OpenWindow.windowTitle);
		}
	}

	public void handeCloseWindow(Packet101CloseWindow packet101CloseWindow) {
		if(currentInventory.getWindowId() == packet101CloseWindow.windowId){
			this.currentInventory = playerInventory;
			System.out.println("Server closed inventory you were in");
		}
	}

	public void handleWindowItems(Packet104WindowItems packet104WindowItems) {
		if(packet104WindowItems.windowId == 0){
			currentInventory = new PlayerInventory(player, packet104WindowItems.itemStack);
			playerInventory = (PlayerInventory) currentInventory;
		}
	}

	public void handleSetSlot(Packet103SetSlot packet103SetSlot) {
		if(packet103SetSlot.windowId == 0){
			if(packet103SetSlot.itemSlot > -1){
				playerInventory.setItemFromServerAt(packet103SetSlot.itemSlot, packet103SetSlot.myItemStack);
			}
		}else if(currentInventory != null){
			if(packet103SetSlot.itemSlot > -1)
			currentInventory.setItemFromServerAt(packet103SetSlot.itemSlot, packet103SetSlot.myItemStack);
		}
	}






}
