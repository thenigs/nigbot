package com.bignigginit;



public class PlayerInventory implements Inventory{

	private final ItemStack[] armor = new ItemStack[4];
	private final ItemStack[] items = new ItemStack[36];
	private final ItemStack[] crafting = new ItemStack[4];

	private ItemStack craftingOutput = null;
	
	Player player;
	
	private short transactionId = 0;
	
	private int currentHeldSlot = 0;
	
	private ItemStack selectedItem = null;
	
	public PlayerInventory(Player p, ItemStack[] stack){
		player = p;
	}
	
	@Override
	public ItemStack getItemAt(int slot) {
		return slot < 36 ? items[slot] : slot < 40 ? armor[slot - 36] : slot < 44 ? crafting[slot - 40] : slot == 44 ? craftingOutput : null;
	}

	public ItemStack getArmorAt(int slot) {
		return armor[slot];
	}

	public ItemStack getCraftingAt(int slot) {
		return crafting[slot];
	}

	public ItemStack getCraftingOutput() {
		return craftingOutput;
	}

	@Override
	public void setItemAt(int slot, ItemStack item) {
		if(slot < 36)
			items[slot] = item;
		else if(slot < 40)
			armor[slot - 36] = item;
		else if(slot < 44)
			crafting[slot - 40] = item;
		else if(slot == 44)
			craftingOutput = item;
		System.out.println(player.username + " Set item at " + slot + ": " + item);
	}

	public  void setArmorAt(int slot, ItemStack item) {
		armor[slot] = item;
	}

	public void setCraftingAt(int slot, ItemStack item) {
		crafting[slot] = item;
	}

	public void setCraftingOutput(ItemStack item) {
		craftingOutput = item;
	}
	
	
	
	public void selectItemAt(int slot){
		ItemStack item = getItemAt(slot);
		ItemStack oldSelected = selectedItem;
		if(selectedItem != null) {
			if(item != null) {
				if(item.getId() == selectedItem.getId()) {
					if(item.getStackSize() != 64) {
						int newStackSize = item.getStackSize() + selectedItem.getStackSize();
						item.setStackSize(Math.min(64, newStackSize));
						newStackSize -= 64;
						if(newStackSize > 0)
							selectedItem.setStackSize(newStackSize);
						else
							selectedItem = null;
					}
				} else {
					setItemAt(slot, selectedItem);
					selectedItem = item;
				}
			} else {
				setItemAt(slot, selectedItem);
				selectedItem = null;
			}
		} else if(item != null) {
			setItemAt(slot, null);
			selectedItem = item;
		}
		System.out.println("player: Clicked at " + slot + " item: " + item + " selected: " + oldSelected);
		
		player.getBot().getNetHandler().writeInventoryChange(new InventoryChange(getWindowId(),getServerSlotFor(slot), 0, transactionId++, item, false));
		delay();
	}



	@Override
	public ItemStack getSelectedItem() {
		return this.selectedItem;
	}

	@Override
	public void dropSelectedItem() {
		selectedItem = null;
		player.getBot().getNetHandler().writeInventoryChange(new InventoryChange(getWindowId(), -999, 0, transactionId++, null, true));
		System.out.println("Dropping selected item");
		delay();
	}

	@Override
	public void close() {
		
	}

	@Override
	public int getWindowId() {
		return 0;
	}

	@Override
	public void setItemFromServerAt(int serverSlot, ItemStack item) {
		setItemAt(getClientSlotFor(serverSlot), item);
	}
	
	private int getClientSlotFor(int serverSlot) {
		if(serverSlot > 35)
			return serverSlot - 36;
		if(serverSlot == 0)
			return 44;
		if(serverSlot < 5)
			return serverSlot + 39;
		if(serverSlot < 9)
			return serverSlot + 31;
		return serverSlot;
	}

	private int getServerSlotFor(int clientSlot) {
		if(clientSlot < 9)
			return 36 + clientSlot;
		else if(clientSlot > 43)
			return clientSlot - 44;
		else if(clientSlot > 39)
			return clientSlot - 39;
		else if(clientSlot > 35)
			return clientSlot - 31;
		return clientSlot;
	}
	
	public boolean contains(int... ids) {
		boolean air = false;
		for(int id : ids)
			if(id == 0)
				air = true;
		for(int slot = 0; slot < 36; slot++)
			if(items[slot] != null) {
				for(int id : ids)
					if(items[slot].getId() == id)
						return true;
			} else if(air)
				return true;
		return false;
	}

	public int getCount(int id) {
		int amount = 0;
		for(int slot = 0; slot < 36; slot++)
			if(items[slot] != null && items[slot].getId() == id)
				amount += items[slot].getStackSize();
		return amount;
	}

	public ItemStack getFirstItem(int id) {
		for(int slot = 0; slot < 36; slot++)
			if(items[slot] != null && items[slot].getId() == id)
				return items[slot];
		return null;
	}

	public int getFirstSlot(int id) {
		for(int slot = 0; slot < 36; slot++)
			if(items[slot] != null ? items[slot].getId() == id : id == 0)
				return slot;
		return -1;
	}
	
	public void selectArmorAt(int slot) {
		selectItemAt(slot + 36);
	}

	public void selectCraftingAt(int slot) {
		selectCraftingAt(slot);
	}

	

	public void selectCraftingOutput() {
		selectItemAt(44);
	}
	
	public void equipIdFromInventory(int id){

		int slot = getFirstSlot(id);

		if(slot == -1){
			System.out.println("No item with that id in inventory");
			return;
		}

		selectItemAt(slot);



		selectItemAt(0);


		if(getSelectedItem() != null){
			selectItemAt(slot);
		}

		
	}
	
	public void dropFirstById(int id){

		int slot = getFirstSlot(id);
		
		if(slot == -1){
			System.out.println("No items by that id in inventory");
			return;
		}
		
		selectItemAt(slot);
		
		
		player.getBot().getNetHandler().writeInventoryChange(new InventoryChange(getWindowId(), -999, 0, transactionId++, null, true));
		System.out.println("Dropping " + id);
		delay();
	}
	
	private void delay() {
		
			try {
				Thread.sleep(250);
			} catch(InterruptedException exception) {
				exception.printStackTrace();
			}
		

	}

	@Override
	public void takeFirst(int id) {
		
	}

	@Override
	public void onPacket106() {
		this.selectedItem = null;
	}
}
