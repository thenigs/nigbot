package com.bignigginit;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bignigginit.packets.Packet0KeepAlive;
import com.bignigginit.packets.Packet100OpenWindow;
import com.bignigginit.packets.Packet101CloseWindow;
import com.bignigginit.packets.Packet103SetSlot;
import com.bignigginit.packets.Packet104WindowItems;
import com.bignigginit.packets.Packet106Transaction;
import com.bignigginit.packets.Packet130UpdateSign;
import com.bignigginit.packets.Packet131MapData;
import com.bignigginit.packets.Packet132TileEntityData;
import com.bignigginit.packets.Packet13PlayerLookMove;
import com.bignigginit.packets.Packet16BlockItemSwitch;
import com.bignigginit.packets.Packet17Sleep;
import com.bignigginit.packets.Packet18Animation;
import com.bignigginit.packets.Packet1Login;
import com.bignigginit.packets.Packet200Statistic;
import com.bignigginit.packets.Packet201PlayerInfo;
import com.bignigginit.packets.Packet202PlayerAbilities;
import com.bignigginit.packets.Packet206SetObjective;
import com.bignigginit.packets.Packet207SetScore;
import com.bignigginit.packets.Packet208SetDisplayObjective;
import com.bignigginit.packets.Packet209SetPlayerTeam;
import com.bignigginit.packets.Packet20NamedEntitySpawn;
import com.bignigginit.packets.Packet22Collect;
import com.bignigginit.packets.Packet23VehicleSpawn;
import com.bignigginit.packets.Packet24MobSpawn;
import com.bignigginit.packets.Packet250CustomPayload;
import com.bignigginit.packets.Packet252SharedKey;
import com.bignigginit.packets.Packet253ServerAuthData;
import com.bignigginit.packets.Packet255Kick;
import com.bignigginit.packets.Packet25EntityPainting;
import com.bignigginit.packets.Packet26EntityExpOrb;
import com.bignigginit.packets.Packet28EntityVelocity;
import com.bignigginit.packets.Packet29DestroyEntity;
import com.bignigginit.packets.Packet30Entity;
import com.bignigginit.packets.Packet31RelEntityMove;
import com.bignigginit.packets.Packet32EntityLook;
import com.bignigginit.packets.Packet33RelEntityMoveLook;
import com.bignigginit.packets.Packet34EntityTeleport;
import com.bignigginit.packets.Packet35EntityHeadRotation;
import com.bignigginit.packets.Packet38EntityStatus;
import com.bignigginit.packets.Packet39AttachEntity;
import com.bignigginit.packets.Packet3Chat;
import com.bignigginit.packets.Packet40EntityMetadata;
import com.bignigginit.packets.Packet41EntityEffect;
import com.bignigginit.packets.Packet44UpdateAttributes;
import com.bignigginit.packets.Packet43Experience;
import com.bignigginit.packets.Packet4UpdateTime;
import com.bignigginit.packets.Packet51MapChunk;
import com.bignigginit.packets.Packet52MultiBlockChange;
import com.bignigginit.packets.Packet53BlockChange;
import com.bignigginit.packets.Packet55BlockDetroy;
import com.bignigginit.packets.Packet56MapChunks;
import com.bignigginit.packets.Packet54PlayNoteBlock;
import com.bignigginit.packets.Packet5Inventory;
import com.bignigginit.packets.Packet61DoorChange;
import com.bignigginit.packets.Packet62NamedSoundEffect;
import com.bignigginit.packets.Packet6SpawnPosition;
import com.bignigginit.packets.Packet70GameEvent;
import com.bignigginit.packets.Packet71Weather;
import com.bignigginit.packets.Packet8UpdateHealth;
import com.bignigginit.packets.Packet9Respawn;

public class PacketHandler {

	private  Map<Integer,Packet> packets = new HashMap(); 

	private  List<Packet> packetList = Arrays.asList(new Packet[] {
			
	new Packet252SharedKey(),
	new Packet253ServerAuthData(),
	new Packet1Login(),
	new Packet6SpawnPosition(),
	new Packet13PlayerLookMove(),
	new Packet0KeepAlive(),
	new Packet250CustomPayload(),
	new Packet202PlayerAbilities(),
	new Packet16BlockItemSwitch(),
	new Packet4UpdateTime(),
	new Packet3Chat(),
	new Packet201PlayerInfo(),
	new Packet104WindowItems(),
	new Packet103SetSlot(),
	new Packet70GameEvent(),
	new Packet255Kick(),
	new Packet56MapChunks(),
	new Packet132TileEntityData(),
	new Packet20NamedEntitySpawn(),
	new Packet40EntityMetadata(),
	new Packet44UpdateAttributes(),
	new Packet130UpdateSign(),
	new Packet35EntityHeadRotation(),
	new Packet30Entity(),
	new Packet31RelEntityMove(),
	new Packet33RelEntityMoveLook(),
	new Packet32EntityLook(),
	new Packet5Inventory(),
	new Packet34EntityTeleport(),
	new Packet28EntityVelocity(),
	new Packet29DestroyEntity(),
	new Packet18Animation(),
	new Packet62NamedSoundEffect(),
	new Packet131MapData(),		
	new Packet24MobSpawn(),
	new Packet23VehicleSpawn(),
	new Packet55BlockDetroy(),
	new Packet53BlockChange(),
	new Packet52MultiBlockChange(),
	new Packet38EntityStatus(),
	new Packet61DoorChange(),
	new Packet209SetPlayerTeam(),
	new Packet25EntityPainting(),
	new Packet41EntityEffect(),
	new Packet39AttachEntity(),
	new Packet8UpdateHealth(),
	new Packet43Experience(),
	new Packet22Collect(),
	new Packet200Statistic(),
	new Packet51MapChunk(),
	new Packet9Respawn(),
	new Packet71Weather(),
	new Packet26EntityExpOrb(),
	new Packet54PlayNoteBlock(),
	new Packet101CloseWindow(),
	new Packet17Sleep(),
	new Packet206SetObjective(),
	new Packet208SetDisplayObjective(),
	new Packet207SetScore(),
	new Packet106Transaction(),
	new Packet100OpenWindow()
	
	});
	

	public PacketHandler(){
		addPackets();
	}
	
	private void addPackets(){
		for(Packet p:packetList){
			packets.put(p.getPacketId(), p);
		}
		
		
	}
	
	public  Packet readPacket(DataInputStream in, TcpReader reader) throws IOException
    {

        Packet packet = null;

        int j;

        try
        {
            j = in.readUnsignedByte();

            int id = Integer.valueOf(j);
            
           // System.out.println(id);

            packet = getPacket(id);
            
           
            if(packet == null){
            	reader.setReading(false);
            	System.out.println("Unhandled Packet: " + id);
            }
            
           

            

        }
        catch (EOFException eofexception)
        {
            return null;
        }

        
        
        return packet;
    }
	
	public  Packet getPacket(int id) {
		Packet p = null;
		
		p = packets.get(id);
		
		return p;
	}
}
