package com.bignigginit;



public class ChestInventory implements Inventory{

	private final MinecraftBot bot;
	private final ItemStack[] items;
	private final ItemStack[] inventory = new ItemStack[36];
	private final int id;

	private ItemStack selectedItem = null;

	public ChestInventory(MinecraftBot bot, int id, int size) {
		this.bot = bot;
		this.id = id;
		items = new ItemStack[size];
	}
	

	public  ItemStack getItemAt(int slot) {
		return slot < items.length ? items[slot] : inventory[slot - items.length];
	}


	public  void setItemAt(int slot, ItemStack item) {
		
		if(slot < items.length){
			items[slot] = item;
			System.out.println(bot.getPlayer().username + " set chest item at " + slot + ": " + item );
		}
		else{
			inventory[slot - items.length] = item;
			System.out.println(bot.getPlayer().username + " set player item at " + slot + ": " + item );
		}
	}
	
	@Override
	public void setItemFromServerAt(int serverSlot, ItemStack item) {
		setItemAt(serverSlot, item);
	}
	

	@Override
	public void selectItemAt(int slot) {
		
		ItemStack item = getItemAt(slot);
		ItemStack oldSelected = selectedItem;
		
		if(selectedItem != null) {
			if(item != null) {
				if(item.getId() == selectedItem.getId()) {
					if(item.getStackSize() != 64) {
						int newStackSize = item.getStackSize() + selectedItem.getStackSize();
						item.setStackSize(Math.min(64, newStackSize));
						newStackSize -= 64;
						if(newStackSize > 0)
							selectedItem.setStackSize(newStackSize);
						else
							selectedItem = null;
					}
				} else {
					setItemAt(slot, selectedItem);
					selectedItem = item;
				}
			} else {
				setItemAt(slot, selectedItem);
				selectedItem = null;
			}
		} else if(item != null) {
			setItemAt(slot, null);
			selectedItem = item;
		}
		System.out.println("CHEST: Clicked at " + slot + " item: " + item + " selected: " + oldSelected);
		bot.getNetHandler().writeInventoryChange(new InventoryChange(getWindowId(),slot, 0, (short)0, item, false));
	}


	@Override
	public ItemStack getSelectedItem() {
		return selectedItem;
	}


	@Override
	public void dropSelectedItem() {
		selectedItem = null;
		bot.getNetHandler().writeInventoryChange(new InventoryChange(getWindowId(), -999, 0, (short) 0, null, true));
		System.out.println("Dropping selected item");
	}


	@Override
	public void close() {
		bot.getNetHandler().player.closeInventory();
	}


	@Override
	public int getWindowId() {
		return this.id;
	}

	
	public int getFirstSlotWithId(int id){
		
		boolean air = id == 0;
		
		for(int slot = 0;slot < items.length;slot++){
			if(items[slot] != null) {
					if(items[slot].getId() == id)
						return slot;
			} else if(air)
				return slot;
		}
		
		return -1;
		
	}
	
	public boolean contains(int... ids) {
		boolean air = false;
		for(int id : ids)
			if(id == 0)
				air = true;
		for(int slot = 0; slot < items.length; slot++)
			if(items[slot] != null) {
				for(int id : ids)
					if(items[slot].getId() == id)
						return true;
			} else if(air)
				return true;
		return false;
	}
	
	public int firstEmptyInventory(){
		
		for(int i = items.length;i < inventory.length;i ++){
			if(inventory[i] == null){
				return i;
			}
		}
		
		
		return -1;
	}

	public void takeFirst(int id){
		if(contains(id)){
			
			if(firstEmptyInventory() == -1){
				System.out.println("No inventory space");
				return;
			}
			
			int itemSlot = getFirstSlotWithId(id);
			
			int emptySlot = firstEmptyInventory();
			
			selectItemAt(itemSlot);
			
			selectItemAt(emptySlot);
			
		}else{
			System.out.println("No items by that id in chest");
		}
	}


	@Override
	public void onPacket106() {
		this.selectedItem = null;
	}

	
	
}
