package com.bignigginit;

import com.bignigginit.nbt.NBTTagCompound;


public class ItemStack {
	private int id, stackSize, damage;
	private NBTTagCompound stackTagCompound;

	public ItemStack(int id, int stackSize, int damage) {
		this.id = id;
		this.stackSize = stackSize;
		this.damage = damage;
	}

	public int getId() {
		return id;
	}

	public int getStackSize() {
		return stackSize;
	}

	public void setStackSize(int stackSize) {
		this.stackSize = stackSize;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public NBTTagCompound getStackTagCompound() {
		return stackTagCompound;
	}

	public void setStackTagCompound(NBTTagCompound stackTagCompound) {
		this.stackTagCompound = stackTagCompound;
	}

	@Override
	public ItemStack clone() {
		return new ItemStack(id, stackSize, damage);
	}

	@Override
	public String toString() {
		return "ItemStack[" + stackSize + "x " + id + ":"+ damage + "]";
		
	}
}