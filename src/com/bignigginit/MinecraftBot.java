package com.bignigginit;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketAddress;

import com.bignigginit.auth.authenticate.AuthThread;

public class MinecraftBot {

	public boolean isRunning = false;
	
	private TcpConnection connection;
	
	Session session;

	private String serverip;

	private int port;
	
	public boolean parseChat;
	
	boolean shouldmovewait;
	
	//Proxy proxy;
	
	public MinecraftBot(String username, String password, String serverip, int port, Proxy p){
		
		this.serverip = serverip;
		
		this.port = port;
		
		//InetSocketAddress proxyAddress = new InetSocketAddress("85.26.146.170", 80);
		
		//Proxy proxy = new Proxy(Proxy.Type.HTTP,proxyAddress);
		
		AuthThread authThread = new AuthThread(username, password, p);
		
		
		Thread a = new Thread(authThread);
		
		a.start();
		while(a.isAlive()){
		
		}
		
		
		
		
		this.session = authThread.getSession();
		
		if(session != null)
			this.connection = new TcpConnection(serverip, port, this, session, new PacketHandler(), p);
		
		
		
	}
	
	public TcpConnection getConnection(){
		return this.connection;
	}
	
	public TcpReader getReader(){
		return getConnection().getReader();
	}
	
	public TcpWriter getWriter(){
		return getConnection().getWriter();
	}
	
	public NetHandler getNetHandler(){
		return getConnection().getNetHandler();
	}
	
	public Player getPlayer(){
		return getNetHandler().player;
	}
	

	public boolean isParsingChat(){
		return this.parseChat;
	}
	
	
	
	public String getUsername() {
		// TODO Auto-generated method stub
		return session.getUsername();
	}

	

}
