package com.bignigginit;



import com.bignigginit.DirectionUtil.Direction;
import com.bignigginit.packets.Packet101CloseWindow;
import com.bignigginit.packets.Packet10Flying;
import com.bignigginit.packets.Packet11PlayerPosition;
import com.bignigginit.packets.Packet12PlayerLook;
import com.bignigginit.packets.Packet13PlayerLookMove;
import com.bignigginit.packets.Packet14BlockDig;
import com.bignigginit.packets.Packet15Place;



public class Player {

	private float health;
	
	double xPos;
	double yPos;
	double stance;
	double zPos;
	
	float yaw;
	float pitch;
	
	private boolean onGround;
	
	public boolean inChest;
	
	DirectionUtil directionUtil;
	
	MinecraftBot bot;
	
	public String username;

	public Player(MinecraftBot b, double x, double stance, double y,  double z, float yaw, float pitch, boolean ong){
		this.xPos = x;
		this.stance = stance;
		this.yPos = y ;
		this.zPos = z;
		this.yaw = yaw;
		this.pitch = pitch;
		this.setOnGround(ong);
		this.bot = b;
		health = 20;
		faceNorth();
		directionUtil = new DirectionUtil();
		this.username = b.getUsername();
	}

	public Direction direction;

	private double oldx;

	private double oldy;

	private double oldz;

	private float oldYaw;

	private float oldPitch;
	
	public String following;
	
	boolean shouldFall;
	
	public void face(Direction d){
		switch (d){
			case  NORTH : faceNorth();
			case SOUTH: faceSouth();
			case EAST: faceEast();
			case WEST: faceWest();
		}
		
		this.direction = d;
	}
	
	
	
	private void faceNorth(){
		setYaw(180);
	}
	
	private void faceSouth(){
		setYaw(0);
	}
	
	private void faceEast(){
		setYaw(-90);
	}
	
	private void faceWest(){
		setYaw(90);
	}
	
	
	
	public double getX() {
		return xPos;
	}

	public void setX(double x) {
		this.xPos = x;
	}

	public double getY() {
		return yPos;
	}

	public void setY(double y) {
		this.yPos = y;
	}

	public double getZ() {
		return zPos;
	}

	public void setZ(double z) {
		this.zPos = z;
	}

	public double getStance() {
		return stance;
	}

	public void setStance(double stance) {
		this.stance = stance;
	}

	public float getYaw() {
		return yaw;
	}

	public void setYaw(float yaw) {
		this.yaw = yaw;
	}

	public float getPitch() {
		return pitch;
	}

	public void setPitch(float pitch) {
		this.pitch = pitch;
	}

	public boolean isOnGround() {
		return onGround;
	}

	public void setOnGround(boolean onGround) {
		this.onGround = onGround;
	}



	public float getHealth() {
		return health;
	}



	public void setHealth(float health) {
		this.health = health;
	}
	
	public void setBot(MinecraftBot b){
		this.bot = b;
	}
	
	public MinecraftBot getBot(){
		return this.bot;
	}



	public void applyGravity() {
		onGround = false;
		yPos -= .4;
		stance -= .4;
	}
	
	
	
	
	
	 
	
	public void moveToEntity( final Entity e){
		
		
		
		new Thread(){
			
			public void run(){

				while(e.getEntityName().equalsIgnoreCase(following)){
					
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					if(!movingforward)
					face(e.getxPos(),e.getyPos(), e.getzPos());
					
					
					boolean collided = false;
					
					DirectionUtil.Direction d = directionUtil.getCollidedDirection(bot) ;

					
					if(d != Direction.NONE){
						
						collided=true;
						//face(d);
						yPos += .6;
						stance += .6;
					}
					
					
					float f = .2F;
					
					
					
					double motionX = (double)(-MathHelper.sin(yaw / 180.0F * (float)Math.PI)  * f);
					double motionZ = (double)(MathHelper.cos(yaw / 180.0F * (float)Math.PI)  * f);
					
					//System.out.println(motionX);
					
					
					
					double parx = e.getxPos();
					boolean atX = xPos >= parx + 1 && xPos <= parx - 1 || xPos <= parx + 1 && xPos >= parx - 1;
					
					double parz = e.getzPos();
					boolean atZ = zPos >= parz + 1 && zPos <= parz - 1 || zPos <= parz + 1 && zPos >= parz - 1;
					
					
					
					if(shouldFall){
						xPos += motionX;
						zPos += motionZ;
						
					}
					
					if(!(atX && atZ) && !shouldFall && !collided && !movingforward){
						xPos += motionX;
						zPos += motionZ;
					}
					
				}
			}
		}.start();
		
		
		
	
}
	boolean movingforward = false;
	/**
	 * 
	 * @param y
	 * -999 for no yaw change
	 */
	public void moveForward(final float y){

		new Thread(){
			public void run(){
				
				int count = 0 ;
				movingforward = true;
				while(true){
					
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					count ++;
					float f = .2F;
					if(y != -999)
					setYaw(y);
					
					double motionX = (double)(-MathHelper.sin(yaw / 180.0F * (float)Math.PI)  * f);
					double motionZ = (double)(MathHelper.cos(yaw / 180.0F * (float)Math.PI)  * f);
					
					xPos += motionX;
					zPos += motionZ;
					
					if(count>5){
						movingforward = false;
						break;
						
					}
					
				}
				
			}
		}.start();
		
	}
	
	

	public void face(double x, double y, double z) {
		yaw = getRotationX(x, y, z);
		pitch = getRotationY(x, y, z);
	}

	private float getRotationX(double x, double y, double z) {
		double d = this.xPos - x;
		double d1 = this.zPos - z;
		return (float) (((Math.atan2(d1, d) * 180D) / Math.PI) + 90) % 360;
	}

	private float getRotationY(double x, double y, double z) {
		double dis1 = y - (this.yPos + 1);
		double dis2 = Math.sqrt(Math.pow(x - this.xPos, 2) + Math.pow(z - this.zPos, 2));
		return (float) ((Math.atan2(dis2, dis1) * 180D) / Math.PI) - 90F;
	}
	
	public void handleMovement() {
		
		new Thread(){
			
			public void run(){
				
				while(getBot().isRunning){
					
					if(getBot().getConnection().disconnect)
						break;
					
					try {
						Thread.sleep(50);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					
					try{
						shouldFall  = directionUtil.canBlockBeWalkedThrough(bot.getNetHandler().world.getBlockAt(xPos, yPos -.2, zPos).getId());

						if(shouldFall)
							applyGravity();
					}catch(Exception e){
						
					}
					
					
					
					
					
					boolean movedPosition = oldx != getX() || oldy != getY() || oldz != getZ();
					
					boolean looked = oldYaw != getYaw() || oldPitch != getPitch();
					
					boolean both = movedPosition && looked;
					
					
					if(both){
						getBot().getWriter().addtoQue(new Packet13PlayerLookMove(getX(), getY(), getStance(), getZ(),getYaw(),getPitch(), isOnGround()));
					}else if(looked){
						getBot().getWriter().addtoQue(new Packet12PlayerLook(getYaw(),getPitch(), isOnGround()));
					}else if(movedPosition){
						getBot().getWriter().addtoQue(new Packet11PlayerPosition(getX(), getY(), getStance(), getZ(), isOnGround()));
					}else{
						getBot().getWriter().addtoQue(new Packet10Flying(isOnGround()));
					}
					
					oldx = getX();
					oldy = getY();
					oldz = getZ();
					
					oldYaw = getYaw();
					oldPitch = getPitch();
					
				}
				
			}
			
		}.start();
		
		
	}

	
	 public double getDistance(double par1, double par3, double par5)
	 {
	        double d3 = this.xPos - par1;
	        double d4 = this.yPos - par3;
	        double d5 = this.zPos - par5;
	        return (double)MathHelper.sqrt_double(d3 * d3 + d4 * d4 + d5 * d5);
	 }
	
	public void placeBlock(int x, int y, int z){
		
		int face = getPlacementBlockFaceAt(getBot().getNetHandler().world, new BlockLocation(x, y, z));
		
		BlockLocation b = getOffsetBlock(new BlockLocation(x, y, z), face);
		
		
		if(b == null){
			System.out.println("Nowhere to place block on");
			return;
		}
		
		if(getDistance(x, y, z) < 5){
			face(x, y, z);
			Packet15Place place = new Packet15Place();
			place.xPosition = b.getX();
			place.yPosition = b.getY();
			place.zPosition = b.getZ();
			place.direction = face;
			place.itemStack = getBot().getNetHandler().playerInventory.getItemAt(36);
			place.xOffset = 0;
			place.yOffset = 0;
			place.zOffset = 0;

			getBot().getNetHandler().addToQue(place);
		}else{
			System.out.println("Block too far away");
		}
		
	}
	
	public void clickBlock(int x, int y, int z){
		if(getDistance(x, y, z) < 5){
			face(x, y, z);
			Packet15Place place = new Packet15Place();
			place.xPosition = x;
			place.yPosition = y;
			place.zPosition = z;
			place.direction = 0;
			place.itemStack = getBot().getNetHandler().playerInventory.getItemAt(36);
			place.xOffset = 0;
			place.yOffset = 0;
			place.zOffset = 0;

			getBot().getNetHandler().addToQue(place);
		}else{
			System.out.println("Block too far away");
		}
	}
	
	public void closeInventory(){
		Packet101CloseWindow closeWindow = new Packet101CloseWindow();
		closeWindow.windowId = bot.getNetHandler().currentInventory.getWindowId();
		bot.getNetHandler().addToQue(closeWindow);
		bot.getNetHandler().currentInventory = bot.getNetHandler().playerInventory;
	}
	
	private BlockLocation getOffsetBlock(BlockLocation location, int face) {
		int x = location.getX(), y = location.getY(), z = location.getZ();
		switch(face) {
		case 0:
			y++;
			break;
		case 1:
			y--;
			break;
		case 2:
			z++;
			break;
		case 3:
			z--;
			break;
		case 4:
			x++;
			break;
		case 5:
			x--;
			break;
		default:
			return null;
		}
		return new BlockLocation(x, y, z);
	}
	
	
	private static byte getPlacementBlockFaceAt(World world, BlockLocation location) {
		int x = location.getX(), y = location.getY(), z = location.getZ();
		if(isPlaceable(world.getBlockIdAt(x, y - 1, z)))
			return 1;
		else if(isPlaceable(world.getBlockIdAt(x, y, z + 1)))
			return 2;
		else if(isPlaceable(world.getBlockIdAt(x, y, z - 1)))
			return 3;
		else if(isPlaceable(world.getBlockIdAt(x + 1, y, z)))
			return 4;
		else if(isPlaceable(world.getBlockIdAt(x - 1, y, z)))
			return 5;
		else if(isPlaceable(world.getBlockIdAt(x, y + 1, z)))
			return 0;
		return -1;
	}



	private static boolean isPlaceable(int blockIdAt) {
		return blockIdAt != 0;
	}
	
	public void selectItem(final int slot){
		bot.getNetHandler().currentInventory.selectItemAt(slot);
		
	}
	
	public void digBlock(BlockLocation l ){
		Packet14BlockDig dig = new Packet14BlockDig(0, l.getX(), l.getY(), l.getZ(), 0);
		
		bot.getNetHandler().addToQue(dig);
		
		dig.status = 2;
		
		bot.getNetHandler().addToQue(dig);
	}

	
	
}
