package com.bignigginit;


import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream.PutField;
import java.io.OutputStream;
import java.net.Proxy;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.crypto.SecretKey;




















import sun.reflect.generics.tree.BottomSignature;

import com.bignigginit.packets.Packet0KeepAlive;
import com.bignigginit.packets.Packet10Flying;
import com.bignigginit.packets.Packet11PlayerPosition;
import com.bignigginit.packets.Packet13PlayerLookMove;
import com.bignigginit.packets.Packet1Login;
import com.bignigginit.packets.Packet205ClientCommand;
import com.bignigginit.packets.Packet252SharedKey;
import com.bignigginit.packets.Packet253ServerAuthData;
import com.bignigginit.packets.Packet255Kick;
import com.bignigginit.packets.Packet2ClientProtocol;



 

public class TcpConnection {
	
	private String serverip;
	
	private int serverport;
	
	private MinecraftBot minecraftBot;
	
	private Socket socket;

	
	private TcpReader reader;
	private TcpWriter writer;
	
	
	private DataInputStream inputStream;
	private DataOutputStream outputStream;
	
	SecretKey sharedKeyForEncryption;

	private NetHandler theNetHandler;
	
	public boolean shouldKeepAlive;
	
	public Session session;
	
	public boolean isLoggedIn;
	
	PacketHandler packetHandler;
	
	public boolean disconnect;
	
	public TcpConnection(String ip, int port,MinecraftBot bot, Session s, PacketHandler p, Proxy proxy){
		
		
		this.session = s;
		
		this.serverip = ip;
		this.serverport = port;
		this.minecraftBot = bot;
		
		this.packetHandler = p;
		
		
		
		
		

	}
	
	
	
	
	public void connect(){
		System.out.println("Connecting " + getBot().getUsername());
		
		try {
			this.socket = new Socket(serverip, serverport);
		
		
		this.theNetHandler = new NetHandler(this);
		
		if(this.socket != null){

				
				inputStream = new DataInputStream(socket.getInputStream());
				outputStream = new DataOutputStream(socket.getOutputStream());
				
				reader = new TcpReader(minecraftBot, inputStream,theNetHandler, this, this.packetHandler);
				writer = new TcpWriter(minecraftBot, outputStream,theNetHandler, this);
				
				getBot().isRunning = true;
				new Thread(reader).start();
				new Thread(writer).start();
				
				

			
		}
		
		
			getBot().isRunning = true;
			Packet.writePacket(new Packet2ClientProtocol(78, this.minecraftBot.getUsername(), this.serverip, this.serverport), new DataOutputStream(socket.getOutputStream()));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void disconnect(){
		System.out.println("Disconnecting " + getBot().getUsername());
		getWriter().addtoQue(new Packet255Kick("Quitting"));
		getBot().isRunning = false;
		getReader().isReading = false;
		disconnect = true;
		
	}
	
	 public MinecraftBot getBot(){
		 return this.minecraftBot; 
	 }
	
	
	public TcpReader getReader(){
		return this.reader;
	}
	
	public TcpWriter getWriter(){
		return this.writer;
	}
	
	public NetHandler getNetHandler(){
		return this.theNetHandler;
	}
	
	
	
	

	
	public void startKeepAliveTick(){
		this.shouldKeepAlive = true;
	}
	
	public void keepAlive(){
		getWriter().addtoQue(new Packet0KeepAlive(0));
		
		
	}
	
	

	
}
