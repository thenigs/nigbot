package com.bignigginit;



public interface Inventory {

	public ItemStack getItemAt(int slot);

	public void setItemAt(int slot, ItemStack item);
	
	public void setItemFromServerAt(int serverSlot, ItemStack item);
	
	public void selectItemAt(int slot);

	public ItemStack getSelectedItem();

	public void dropSelectedItem();

	public void close();

	public int getWindowId();
	
	public void takeFirst(int id);
	
	public void onPacket106();
	
}
