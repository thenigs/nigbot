package com.bignigginit;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Message {

	 	@Expose
	    @SerializedName("text")
	    private String message;
	 	
	 	
	 	public String getMessage(){
	 		return this.message;
	 	}
	 	
	
}
