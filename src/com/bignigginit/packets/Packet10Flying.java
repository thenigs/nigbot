package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet10Flying extends Packet{

	

    /** True if the client is on the ground. */
    public boolean onGround;

   

    public Packet10Flying() {}

	
	public Packet10Flying(boolean onGround) {
		this.onGround = onGround;
	}


	@Override
	public void readPacketData(DataInput datainput) throws IOException {
		this.onGround = datainput.readUnsignedByte() != 0;
	}

	@Override
	public void writePacketData(DataOutput dataoutput) throws IOException {
		dataoutput.write(onGround ? 1 : 0);
	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		theNetHandler.handleFlying(this);
	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 10;
	}

}
