package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;

public class Packet32EntityLook extends Packet30Entity {

	
	public void readPacketData(DataInput par1DataInput) throws IOException
    {
        super.readPacketData(par1DataInput);
        this.yaw = par1DataInput.readByte();
        this.pitch = par1DataInput.readByte();
    }
	
	@Override
	public void writePacketData(DataOutput dataoutput) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 32;
	}
	
	
}
