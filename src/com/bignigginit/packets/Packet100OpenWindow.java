package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.InventoryType;
import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet100OpenWindow extends Packet {

	public int windowId;
	public InventoryType inventoryType;
	public String windowTitle;
	public int slotsCount;
	public boolean flag;
	public int entityId;

	
	@Override
	public void readPacketData(DataInput in) throws IOException {
		windowId = in.readByte() & 255;
		inventoryType = InventoryType.byId(in.readByte() & 255);
		windowTitle = readString(in, 32);
		slotsCount = in.readByte() & 255;
		flag = in.readBoolean();
		if(inventoryType == InventoryType.ANIMAL_CHEST)
			entityId = in.readInt();
	}

	@Override
	public void writePacketData(DataOutput out) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		theNetHandler.handleOpenWindow(this);
	}

	@Override
	public int getPacketId() {
		return 100;
	}

}
