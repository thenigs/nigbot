package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.ItemStack;
import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet102WindowClick extends Packet{

	public int windowId;
	public int slot;
	public int button;
	public short action;
	public ItemStack item;
	public boolean shift;
	
	@Override
	public void readPacketData(DataInput in) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void writePacketData(DataOutput out) throws IOException {
		out.writeByte(windowId);
		out.writeShort(slot);
		out.writeByte(button);
		out.writeShort(action);
		out.writeByte(shift ? 1 : 0);
		writeItemStack(item, out);
	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 102;
	}

}
