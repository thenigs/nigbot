package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet3Chat extends Packet{

	 public String message;
	    private boolean isServer;

	    public Packet3Chat()
	    {
	        this.isServer = true;
	    }

	    

	   

	    public Packet3Chat(String par1Str)
	    {
	        this(par1Str, true);
	    }

	    public Packet3Chat(String par1Str, boolean par2)
	    {
	        this.isServer = true;

	        if (par1Str.length() > 32767)
	        {
	            par1Str = par1Str.substring(0, 32767);
	        }

	        this.message = par1Str;
	        this.isServer = par2;
	    }

	    /**
	     * Abstract. Reads the raw packet data from the data stream.
	     */
	    public void readPacketData(DataInput par1DataInput) throws IOException
	    {
	        this.message = readString(par1DataInput, 32767);
	    }

	    /**
	     * Abstract. Writes the raw packet data to the data stream.
	     */
	    public void writePacketData(DataOutput par1DataOutput) throws IOException
	    {
	        writeString(this.message, par1DataOutput);
	    }

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 3;
	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		theNetHandler.handleChat(this);
	}

}
