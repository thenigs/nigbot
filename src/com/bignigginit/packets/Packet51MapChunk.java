package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet51MapChunk extends Packet{

	public int x;
	public int z;
	public int bitmask;
	public int additionalBitmask;
	public boolean biomes;
	public byte[] chunkData;
	
	@Override
	public void readPacketData(DataInput in) throws IOException {
		x = in.readInt();
		z = in.readInt();
		biomes = in.readBoolean();
		bitmask = in.readShort();
		additionalBitmask = in.readShort();
		int tempLength = in.readInt();

		byte[] compressedChunkData = new byte[tempLength];
		in.readFully(compressedChunkData, 0, tempLength);
		int i = 0;

		for(int j = 0; j < 16; j++)
			i += bitmask >> j & 1;

		int k = 12288 * i;

		if(biomes)
			k += 256;

		chunkData = new byte[k];
		Inflater inflater = new Inflater();
		inflater.setInput(compressedChunkData, 0, tempLength);

		try {
			inflater.inflate(chunkData);
		} catch(DataFormatException dataformatexception) {
			chunkData = null;
		} catch(OutOfMemoryError error) {
			System.gc();
			try {
				inflater.end();

				inflater = new Inflater();
				inflater.setInput(compressedChunkData, 0, tempLength);

				inflater.inflate(chunkData);
			} catch(DataFormatException dataformatexception) {
				chunkData = null;
			} catch(OutOfMemoryError error2) {
				chunkData = null;
			}
		} finally {
			inflater.end();
		}
	}

	@Override
	public void writePacketData(DataOutput out) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		theNetHandler.handleChunkLoad(this);
	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 51;
	}

}
