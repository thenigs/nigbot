package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet12PlayerLook extends Packet10Flying {

	public float yaw;
	public float pitch;

	
	public Packet12PlayerLook(float par1, float par2, boolean par3) {
		super(par3);
		yaw = par1;
		pitch = par2;
	}
	
	@Override
	public void readPacketData(DataInput in) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void writePacketData(DataOutput out) throws IOException {
		out.writeFloat(yaw);
		out.writeFloat(pitch);
		super.writePacketData(out);
	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		// TODO Auto-generated method stub

	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 12;
	}

}
