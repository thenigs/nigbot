package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet131MapData extends Packet{

	
	public short itemId;
	public short uniqueId;
	public byte[] itemData;

	
	@Override
	public void readPacketData(DataInput in) throws IOException {
		itemId = in.readShort();
		uniqueId = in.readShort();
		itemData = new byte[in.readShort()];
		in.readFully(itemData);
	}

	@Override
	public void writePacketData(DataOutput out) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 131;
	}

}
