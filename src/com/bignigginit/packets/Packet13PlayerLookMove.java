package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.Main;
import com.bignigginit.NetHandler;

public class Packet13PlayerLookMove extends Packet10Flying {
	
	public double x;
	public double y;
	public double z;
	public double stance;
	public float yaw;
	public float pitch;
	
	public Packet13PlayerLookMove()
    {
    }
	
	public Packet13PlayerLookMove(double x, double y, double stance, double z, float yaw, float pitch, boolean onGround) {
		super(onGround);
		this.x = x;
		this.y = y;
		this.stance = stance;
		this.z = z;
		this.yaw = yaw;
		this.pitch = pitch;
	}
	@Override
	public void readPacketData(DataInput par1DataInput) throws IOException
    {
		
        this.x = par1DataInput.readDouble();
        
        this.stance = par1DataInput.readDouble();
        this.y = par1DataInput.readDouble();
        this.z = par1DataInput.readDouble();
        this.yaw = par1DataInput.readFloat();
        this.pitch = par1DataInput.readFloat();
        super.readPacketData(par1DataInput);
    }

    /**
     * Abstract. Writes the raw packet data to the data stream.
     */
	@Override
    public void writePacketData(DataOutput par1DataOutput) throws IOException
    {
        par1DataOutput.writeDouble(this.x);
        par1DataOutput.writeDouble(this.y);
        par1DataOutput.writeDouble(this.stance);
        par1DataOutput.writeDouble(this.z);
        par1DataOutput.writeFloat(this.yaw);
        par1DataOutput.writeFloat(this.pitch);
        super.writePacketData(par1DataOutput);
    }
	
	public void processPacket(NetHandler theNetHandler) {
		if(Main.movement)
		theNetHandler.handlePlayerLookMove(this);
	}
	
	@Override
	public int getPacketId(){
		return 13;	
	}

}
