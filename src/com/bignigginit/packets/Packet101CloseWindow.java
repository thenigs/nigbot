package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet101CloseWindow extends Packet {

	public int windowId;
	
	@Override
	public void readPacketData(DataInput in) throws IOException {
		windowId = in.readByte();
	}

	@Override
	public void writePacketData(DataOutput out) throws IOException {
		out.writeByte(windowId);
	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		theNetHandler.handeCloseWindow(this);
	}

	@Override
	public int getPacketId() {
		return 101;
	}

}
