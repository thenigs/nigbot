package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet55BlockDetroy extends Packet {

	public int entityId;
	public int x;
	public int y;
	public int z;
	public int destroyStage;
	
	@Override
	public void readPacketData(DataInput in) throws IOException {
		entityId = in.readInt();
		x = in.readInt();
		y = in.readInt();
		z = in.readInt();
		destroyStage = in.readUnsignedByte();
	}

	@Override
	public void writePacketData(DataOutput out) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		//theNetHandler.handeBlockDestroy(this);
	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 55;
	}

}
