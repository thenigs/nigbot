package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet16BlockItemSwitch extends Packet{
	
	/** The block/item id to be equipped. */
    public int id;

    public Packet16BlockItemSwitch() {}

    public Packet16BlockItemSwitch(int par1)
    {
        this.id = par1;
    }

   
    public void readPacketData(DataInput par1DataInput) throws IOException
    {
        this.id = par1DataInput.readShort();
    }

  
    public void writePacketData(DataOutput par1DataOutput) throws IOException
    {
        par1DataOutput.writeShort(this.id);
    }

    public void processPacket(NetHandler par1NetHandler)
    {
        par1NetHandler.handleBlockItemSwitch(this);
    }

	

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 16;
	}

}
