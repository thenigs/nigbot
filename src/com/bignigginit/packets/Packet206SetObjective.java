package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet206SetObjective extends Packet{
	private String string1;
	private String string2;
	private int byte1;
	@Override
	public void readPacketData(DataInput in) throws IOException {
		
		
		string1 = readString(in, 16);
		string2 = readString(in, 32);
		byte1 = in.readByte();
	}

	@Override
	public void writePacketData(DataOutput out) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 206;
	}

}
