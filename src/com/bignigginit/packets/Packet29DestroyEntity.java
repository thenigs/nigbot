package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet29DestroyEntity extends Packet {

	public int[] entityIds;
	
	@Override
	public void readPacketData(DataInput in) throws IOException {
		entityIds = new int[in.readByte()];

		for(int var2 = 0; var2 < entityIds.length; var2++)
			entityIds[var2] = in.readInt();
	}

	@Override
	public void writePacketData(DataOutput dataoutput) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		// TODO Auto-generated method stub

	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 29;
	}

}
