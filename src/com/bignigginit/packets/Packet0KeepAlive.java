package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;
import com.bignigginit.TcpConnection;

public class Packet0KeepAlive extends Packet{

	public int randomId;
	
	DataOutputStream outputStream;
	
	public Packet0KeepAlive(){}
	
	public Packet0KeepAlive(int randomId2) {
		this.randomId = randomId2;
	}

	@Override
	public void readPacketData(DataInput datainput) throws IOException {
		this.randomId = datainput.readInt();
		
	}

	@Override
	public void writePacketData(DataOutput dataoutput) throws IOException {
		 dataoutput.writeInt(this.randomId);
	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		theNetHandler.handleKeepAlive(this);
	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 0;
	}

}
