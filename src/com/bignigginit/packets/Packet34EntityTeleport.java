package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet34EntityTeleport extends Packet {

	/** ID of the entity. */
    public int entityId;

    /** X position of the entity. */
    public int xPosition;

    /** Y position of the entity. */
    public int yPosition;

    /** Z position of the entity. */
    public int zPosition;

    /** Yaw of the entity. */
    public byte yaw;

    /** Pitch of the entity. */
    public byte pitch;
	
	@Override
	public void readPacketData(DataInput datainput) throws IOException {
		this.entityId = datainput.readInt();
        this.xPosition = datainput.readInt();
        this.yPosition = datainput.readInt();
        this.zPosition = datainput.readInt();
        this.yaw = datainput.readByte();
        this.pitch = datainput.readByte();
	}

	@Override
	public void writePacketData(DataOutput dataoutput) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		theNetHandler.handleEntityTeleport(this);
	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 34;
	}

}
