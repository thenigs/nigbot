package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet209SetPlayerTeam extends Packet {

	private String string1 = "";
	private String string2 = "";
	private String string3 = "";
	private String string4 = "";
	private List<String> list = new ArrayList<String>();
	private int int1 = 0;
	private int int2;
	
	@Override
	public void readPacketData(DataInput in) throws IOException {
		string1 = readString(in, 16);
		int1 = in.readByte();

		if(int1 == 0 || int1 == 2) {
			string2 = readString(in, 32);
			string3 = readString(in, 16);
			string4 = readString(in, 16);
			int2 = in.readByte();
		}

		if(int1 == 0 || int1 == 3 || int1 == 4) {
			short length = in.readShort();

			for(int i = 0; i < length; i++)
				list.add(readString(in, 16));
		}
	}

	@Override
	public void writePacketData(DataOutput out) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		// TODO Auto-generated method stub

	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 209;
	}

}
