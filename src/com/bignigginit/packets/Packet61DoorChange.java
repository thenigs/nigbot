package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet61DoorChange extends Packet {

	public int sfxID;
	public int auxData;
	public int posX;
	public int posY;
	public int posZ;
	public boolean bool;

	
	@Override
	public void readPacketData(DataInput in) throws IOException {
		sfxID = in.readInt();
		posX = in.readInt();
		posY = in.readByte() & 0xff;
		posZ = in.readInt();
		auxData = in.readInt();
		bool = in.readBoolean();
	}

	@Override
	public void writePacketData(DataOutput out) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		// TODO Auto-generated method stub

	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 61;
	}

}
