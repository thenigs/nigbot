package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet106Transaction extends Packet{

	public int windowId;
	public short shortWindowId;
	public boolean accepted;

	@Override
	public void readPacketData(DataInput in) throws IOException {
		windowId = in.readByte();
		shortWindowId = in.readShort();
		accepted = in.readByte() != 0;
	}

	@Override
	public void writePacketData(DataOutput out) throws IOException {
		out.writeByte(windowId);
		out.writeShort(shortWindowId);
		out.writeByte(accepted ? 1 : 0);
	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		theNetHandler.handleTransaction(this);
	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 106;
	}

}
