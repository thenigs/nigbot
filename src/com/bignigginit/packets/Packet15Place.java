package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.ItemStack;
import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet15Place extends Packet {

	public int xPosition, yPosition, zPosition;
	public float xOffset, yOffset, zOffset;

	public int direction;
	public ItemStack itemStack;
	
	@Override
	public void readPacketData(DataInput in) throws IOException {

	}

	@Override
	public void writePacketData(DataOutput out) throws IOException {
		out.writeInt(xPosition);
		out.write(yPosition);
		out.writeInt(zPosition);
		out.write(direction);
		writeItemStack(itemStack, out);
		out.write((int) (xOffset * 16F));
		out.write((int) (yOffset * 16F));
		out.write((int) (zOffset * 16F));
	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		
	}

	@Override
	public int getPacketId() {
		return 15;
	}

}
