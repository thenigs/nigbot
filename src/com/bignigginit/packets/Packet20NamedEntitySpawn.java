package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet20NamedEntitySpawn extends Packet{

	public int entityId;
	public String name;

	public int xPosition;
	public int yPosition;
	public int zPosition;
	public byte rotation;
	public byte pitch;

	public int currentItem;
	
	@Override
	public void readPacketData(DataInput in) throws IOException {
		entityId = in.readInt();
		name = readString(in, 16);
		xPosition = in.readInt();
		yPosition = in.readInt();
		zPosition = in.readInt();
		rotation = in.readByte();
		pitch = in.readByte();
		currentItem = in.readShort();
		readWatchableObjects(in);
	}

	@Override
	public void writePacketData(DataOutput dataoutput) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		theNetHandler.handeEntitySpawn(this);
	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 20;
	}
	

}
