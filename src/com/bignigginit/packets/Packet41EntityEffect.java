package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet41EntityEffect extends Packet {

	public int entityId;
	public byte effectId;

	public byte effectAmp;
	public short duration;

	
	@Override
	public void readPacketData(DataInput in) throws IOException {
		entityId = in.readInt();
		effectId = in.readByte();
		effectAmp = in.readByte();
		duration = in.readShort();
	}

	@Override
	public void writePacketData(DataOutput out) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		// TODO Auto-generated method stub

	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 41;
	}

}
