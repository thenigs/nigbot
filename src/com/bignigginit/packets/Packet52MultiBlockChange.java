package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet52MultiBlockChange extends Packet {

	public int xPosition;
	public int zPosition;

	public byte[] metadataArray;
	public int size;
	
	@Override
	public void readPacketData(DataInput in) throws IOException {
		xPosition = in.readInt();
		zPosition = in.readInt();
		size = in.readShort() & 0xffff;
		int i = in.readInt();

		if(i > 0) {
			metadataArray = new byte[i];
			in.readFully(metadataArray);
		}
	}

	@Override
	public void writePacketData(DataOutput out) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		// TODO Auto-generated method stub

	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 52;
	}

}
