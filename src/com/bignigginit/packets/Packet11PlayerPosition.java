package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;




public class Packet11PlayerPosition extends Packet10Flying{
	
		public Packet11PlayerPosition()
	    {
		 
	    }

	 	public double x;
		public double y;
		public double z;
		public double stance;
	   
	    public Packet11PlayerPosition(double par1, double par3, double par5, double par7, boolean par9)
	    {
	        this.x = par1;
	        this.y = par3;
	        this.stance = par5;
	        this.z = par7;
	        this.onGround = par9;

	    }

	    /**
	     * Abstract. Reads the raw packet data from the data stream.
	     */
	    public void readPacketData(DataInput par1DataInput) throws IOException
	    {
	        this.x = par1DataInput.readDouble();
	        this.y = par1DataInput.readDouble();
	        this.stance = par1DataInput.readDouble();
	        this.z = par1DataInput.readDouble();
	        super.readPacketData(par1DataInput);
	    }

	    /**
	     * Abstract. Writes the raw packet data to the data stream.
	     */
	    public void writePacketData(DataOutput par1DataOutput) throws IOException
	    {
	        par1DataOutput.writeDouble(this.x);
	        par1DataOutput.writeDouble(this.y);
	        par1DataOutput.writeDouble(this.stance);
	        par1DataOutput.writeDouble(this.z);
	        super.writePacketData(par1DataOutput);
	    }

	    @Override
		public int getPacketId(){
			return 11;	
		}
	    
}
