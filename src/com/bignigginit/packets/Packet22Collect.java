package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet22Collect extends Packet {

	public int collectedEntityId;
	public int collectorEntityId;

	
	@Override
	public void readPacketData(DataInput in) throws IOException {
		collectedEntityId = in.readInt();
		collectorEntityId = in.readInt();
	}

	@Override
	public void writePacketData(DataOutput out) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		// TODO Auto-generated method stub

	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 22;
	}

}
