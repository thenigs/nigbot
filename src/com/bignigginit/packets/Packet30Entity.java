package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet30Entity extends Packet{

	
	 /** The ID of this entity. */
    public int entityId;

    /** The X axis relative movement. */
    public byte xPosition;

    /** The Y axis relative movement. */
    public byte yPosition;

    /** The Z axis relative movement. */
    public byte zPosition;

    /** The X axis rotation. */
    public byte yaw;

    /** The Y axis rotation. */
    public byte pitch;

    /** Boolean set to true if the entity is rotating. */
    public boolean rotating;
	
	@Override
	public void readPacketData(DataInput datainput) throws IOException {
		this.entityId = datainput.readInt();
	}

	@Override
	public void writePacketData(DataOutput dataoutput) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		theNetHandler.handleEntity(this);
	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 30;
	}

}
