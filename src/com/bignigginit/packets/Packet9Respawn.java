package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;






import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet9Respawn extends Packet {

	public int dimension;
	
	 public byte difficultySetting;
	 
	 public int gameType;
	 
	 public String terrainType;
	 
	 public int worldHeight;
	
	@Override
	public void readPacketData(DataInput in) throws IOException {
		this.dimension = in.readInt();
        this.difficultySetting = in.readByte();
        this.gameType = in.readByte();
        this.worldHeight = in.readShort();
        String s = readString(in, 16);
        this.terrainType = s;

        

	}

	@Override
	public void writePacketData(DataOutput out) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		theNetHandler.handleRespawn(this);
	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 9;
	}

}
