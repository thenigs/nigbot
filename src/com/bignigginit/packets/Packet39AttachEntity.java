package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet39AttachEntity extends Packet {

	public int entityId;
	public int vehicleEntityId;
	public boolean leashed;
	
	@Override
	public void readPacketData(DataInput in) throws IOException {
		entityId = in.readInt();
		vehicleEntityId = in.readInt();
		leashed = in.readUnsignedByte() == 1;
	}

	@Override
	public void writePacketData(DataOutput out) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		// TODO Auto-generated method stub

	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 39;
	}

}
