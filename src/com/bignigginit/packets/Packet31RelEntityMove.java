package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;

public class Packet31RelEntityMove extends Packet30Entity{

	@Override
	public void readPacketData(DataInput datainput) throws IOException {
		super.readPacketData(datainput);
        this.xPosition = datainput.readByte();
        this.yPosition = datainput.readByte();
        this.zPosition = datainput.readByte();

        
	}

	@Override
	public void writePacketData(DataOutput dataoutput) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		super.processPacket(theNetHandler);
	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 31;
	}
	
}
