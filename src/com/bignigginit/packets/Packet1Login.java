package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;
import com.bignigginit.TcpConnection;
import com.bignigginit.fml.FMLNetworkHandler;


public class Packet1Login extends Packet
{
    /** The player's entity ID */
    public int clientEntityId;
    public String terrainType;
    public boolean hardcoreMode;
    public int gameType;

    /** -1: The Nether, 0: The Overworld, 1: The End */
    public int dimension;

    /** The difficulty setting byte. */
    public byte difficultySetting;

    /** Defaults to 128 */
    public byte worldHeight;

    /** The maximum players. */
    public byte maxPlayers;

    private boolean vanillaCompatible;
    
   

    /**
     * Abstract. Reads the raw packet data from the data stream.
     */
    public void readPacketData(DataInput par1DataInput) throws IOException
    {
    	
    	this.vanillaCompatible = FMLNetworkHandler.vanillaLoginPacketCompatibility();
        this.clientEntityId = par1DataInput.readInt();
        
        String s = readString(par1DataInput, 16);
        this.terrainType = s;

      //  if (this.terrainType == null)
      //  {
      //      this.terrainType = WorldType.DEFAULT;
      //  }

        byte b0 = par1DataInput.readByte();
        this.hardcoreMode = (b0 & 8) == 8;
        int i = b0 & -9;
        this.gameType = i;

        if (vanillaCompatible)
        {
            this.dimension = par1DataInput.readByte();
            
        }
        else
        {
            this.dimension = par1DataInput.readInt();
        }

        this.difficultySetting = par1DataInput.readByte();
        this.worldHeight = par1DataInput.readByte();
        this.maxPlayers = par1DataInput.readByte();
        
        
        
    }

    /**
     * Abstract. Writes the raw packet data to the data stream.
     */
    public void writePacketData(DataOutput par1DataOutput) throws IOException
    {
    	this.vanillaCompatible = FMLNetworkHandler.vanillaLoginPacketCompatibility();
    	
    	par1DataOutput.writeInt(this.clientEntityId);
        writeString(this.terrainType == null ? "" : this.terrainType, par1DataOutput);
        int i = this.gameType;

        if (this.hardcoreMode)
        {
            i |= 8;
        }

        par1DataOutput.writeByte(i);

        if (vanillaCompatible)
        {
            par1DataOutput.writeByte(this.dimension);
        }
        else
        {
            par1DataOutput.writeInt(this.dimension);
        }

        par1DataOutput.writeByte(this.difficultySetting);
        par1DataOutput.writeByte(this.worldHeight);
        par1DataOutput.writeByte(this.maxPlayers);
    }

    /**
     * Passes this Packet on to the NetHandler for processing.
     */
    public void processPacket(NetHandler par1NetHandler)
    {
        par1NetHandler.handleLogin(this);
    }


	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 1;
	}

}
