package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet62NamedSoundEffect extends Packet {

	public String soundName;

	public int xPosition;
	public int yPosition = Integer.MAX_VALUE;
	public int zPosition;

	public float volume;
	public int pitch;
	
	@Override
	public void readPacketData(DataInput in) throws IOException {
		soundName = readString(in, 32);
		xPosition = in.readInt();
		yPosition = in.readInt();
		zPosition = in.readInt();
		volume = in.readFloat();
		pitch = in.readUnsignedByte();
	}

	@Override
	public void writePacketData(DataOutput out) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		// TODO Auto-generated method stub

	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 62;
	}

}
