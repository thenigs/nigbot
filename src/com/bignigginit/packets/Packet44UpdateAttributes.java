package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet44UpdateAttributes extends Packet{

	private int field_111005_a;
    private final List field_111004_b = new ArrayList();
	
	@Override
	public void readPacketData(DataInput datainput) throws IOException {
		this.field_111005_a = datainput.readInt();
        int i = datainput.readInt();

        for (int j = 0; j < i; ++j)
        {
            String s = readString(datainput, 64);
            double d0 = datainput.readDouble();
            ArrayList arraylist = new ArrayList();
            short short1 = datainput.readShort();

            for (int k = 0; k < short1; ++k)
            {
                UUID uuid = new UUID(datainput.readLong(), datainput.readLong());
               /* arraylist.add(new AttributeModifier(uuid, "Unknown synced attribute modifier", */datainput.readDouble();/*,*/ datainput.readByte()/*))*/;
            }

            //this.field_111004_b.add(new Packet44UpdateAttributesSnapshot(this, s, d0, arraylist));
        }
	}

	@Override
	public void writePacketData(DataOutput dataoutput) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 44;
	}

}
