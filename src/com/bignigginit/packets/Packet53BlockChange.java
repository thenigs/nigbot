package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet53BlockChange extends Packet {

	public int xPosition;
	public int yPosition;
	public int zPosition;

	public int type;
	public int metadata;
	
	@Override
	public void readPacketData(DataInput in) throws IOException {
		xPosition = in.readInt();
		yPosition = in.readUnsignedByte();
		zPosition = in.readInt();
		type = in.readShort();
		metadata = in.readUnsignedByte();
	}

	@Override
	public void writePacketData(DataOutput out) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		theNetHandler.handeBlockChange(this);
	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 53;
	}

}
