package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet14BlockDig extends Packet{
	
	
	public int xPosition;
	public int yPosition;
	public int zPosition;

	public int face;
	public int status;

	
	public Packet14BlockDig(int par1, int par2, int par3, int par4, int par5) {
		status = par1;
		xPosition = par2;
		yPosition = par3;
		zPosition = par4;
		face = par5;
	}

	@Override
	public void readPacketData(DataInput in) throws IOException {
		
	}

	@Override
	public void writePacketData(DataOutput out) throws IOException {
		out.write(status);
		out.writeInt(xPosition);
		out.write(yPosition);
		out.writeInt(zPosition);
		out.write(face);
	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		
	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 14;
	}

}
