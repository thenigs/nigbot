package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet205ClientCommand extends Packet{

	public int forceRespawn;
	
	public Packet205ClientCommand(int par1)
    {
        this.forceRespawn = par1;
    }

	
	@Override
	public void readPacketData(DataInput datainput) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void writePacketData(DataOutput dataoutput) throws IOException {
		dataoutput.writeByte(this.forceRespawn & 255);
		
	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 205;
	}

}
