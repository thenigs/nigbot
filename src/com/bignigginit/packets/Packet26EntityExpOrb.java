package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet26EntityExpOrb extends Packet{

	public int entityId;

	public int posX;
	public int posY;
	public int posZ;

	public int xpValue;
	
	@Override
	public void readPacketData(DataInput in) throws IOException {
		entityId = in.readInt();
		posX = in.readInt();
		posY = in.readInt();
		posZ = in.readInt();
		xpValue = in.readShort();
	}

	@Override
	public void writePacketData(DataOutput out) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 26;
	}

}
