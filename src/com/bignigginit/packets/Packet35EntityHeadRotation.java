package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet35EntityHeadRotation extends Packet{

	public int entityId;
    public byte headRotationYaw;

	
	@Override
	public void readPacketData(DataInput datainput) throws IOException {
		 this.entityId = datainput.readInt();
	       this.headRotationYaw = datainput.readByte();
	}

	@Override
	public void writePacketData(DataOutput dataoutput) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 35;
	}

}
