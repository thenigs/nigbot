package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet54PlayNoteBlock extends Packet {

	public int xLocation;
	public int yLocation;
	public int zLocation;

	public int instrumentType;
	public int pitch;
	public int blockId;

	
	@Override
	public void readPacketData(DataInput in) throws IOException {
		xLocation = in.readInt();
		yLocation = in.readShort();
		zLocation = in.readInt();
		instrumentType = in.readUnsignedByte();
		pitch = in.readUnsignedByte();
		blockId = in.readShort() & 0xfff;
	}

	@Override
	public void writePacketData(DataOutput out) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		// TODO Auto-generated method stub

	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 54;
	}

}
