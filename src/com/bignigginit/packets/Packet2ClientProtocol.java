package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet2ClientProtocol extends Packet{
	
	private int protocolVersion;
    private String username;
    private String serverHost;
    private int serverPort;

    public Packet2ClientProtocol(int par1, String par2Str, String par3Str, int par4)
    {
        this.protocolVersion = par1;
        this.username = par2Str;
        this.serverHost = par3Str;
        this.serverPort = par4;
    }

	@Override
	public void readPacketData(DataInput datainput) throws IOException {
		
		
	}

	@Override
	public void writePacketData(DataOutput dataoutput) throws IOException {
		dataoutput.writeByte(this.protocolVersion);
        writeString(this.username, dataoutput);
        writeString(this.serverHost, dataoutput);
        dataoutput.writeInt(this.serverPort);
		
	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 2;
	}

}
