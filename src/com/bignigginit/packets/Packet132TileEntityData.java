package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet132TileEntityData extends Packet{

	
	public int xPosition;
	public int yPosition;
	public int zPosition;
	public int actionType;
	
	@Override
	public void readPacketData(DataInput in) throws IOException {
		xPosition = in.readInt();
		yPosition = in.readShort();
		zPosition = in.readInt();
		actionType = in.readByte();
		 readNBTTagCompound(in);
		
	}

	@Override
	public void writePacketData(DataOutput dataoutput) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 132;
	}

}
