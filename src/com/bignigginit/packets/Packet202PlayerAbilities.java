package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;




import com.bignigginit.NetHandler;
import com.bignigginit.Packet;


public class Packet202PlayerAbilities extends Packet{
	
	/** Disables player damage. */
    private boolean disableDamage;

    /** Indicates whether the player is flying or not. */
    private boolean isFlying;

    /** Whether or not to allow the player to fly when they double jump. */
    private boolean allowFlying;

    /**
     * Used to determine if creative mode is enabled, and therefore if items should be depleted on usage
     */
    private boolean isCreativeMode;
    private float flySpeed;
    private float walkSpeed;

    public Packet202PlayerAbilities() {}

    

    /**
     * Abstract. Reads the raw packet data from the data stream.
     */
    public void readPacketData(DataInput par1DataInput) throws IOException
    {
        byte b0 = par1DataInput.readByte();
        this.setDisableDamage((b0 & 1) > 0);
        this.setFlying((b0 & 2) > 0);
        this.setAllowFlying((b0 & 4) > 0);
        this.setCreativeMode((b0 & 8) > 0);
        this.setFlySpeed(par1DataInput.readFloat());
        this.setWalkSpeed(par1DataInput.readFloat());
    }

    /**
     * Abstract. Writes the raw packet data to the data stream.
     */
    public void writePacketData(DataOutput par1DataOutput) throws IOException
    {
        
    }

    @Override
	public void processPacket(NetHandler theNetHandler) {
		// TODO Auto-generated method stub
		
	}

    public boolean getDisableDamage()
    {
        return this.disableDamage;
    }

    /**
     * Sets whether damage is disabled or not.
     */
    public void setDisableDamage(boolean par1)
    {
        this.disableDamage = par1;
    }

    public boolean getFlying()
    {
        return this.isFlying;
    }

    /**
     * Sets whether we're currently flying or not.
     */
    public void setFlying(boolean par1)
    {
        this.isFlying = par1;
    }

    public boolean getAllowFlying()
    {
        return this.allowFlying;
    }

    public void setAllowFlying(boolean par1)
    {
        this.allowFlying = par1;
    }

    public boolean isCreativeMode()
    {
        return this.isCreativeMode;
    }

    public void setCreativeMode(boolean par1)
    {
        this.isCreativeMode = par1;
    }

    public float getFlySpeed()
    {
        return this.flySpeed;
    }

    /**
     * Sets the flying speed.
     */
    public void setFlySpeed(float par1)
    {
        this.flySpeed = par1;
    }


    public float getWalkSpeed()
    {
        return this.walkSpeed;
    }

    /**
     * Sets the walking speed.
     */
    public void setWalkSpeed(float par1)
    {
        this.walkSpeed = par1;
    }



	



	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 202;
	}


}
