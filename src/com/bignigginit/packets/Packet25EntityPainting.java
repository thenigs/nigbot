package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet25EntityPainting extends Packet {

	public int entityId;
	public String title;

	public int xPosition;
	public int yPosition;
	public int zPosition;
	public int direction;
	
	@Override
	public void readPacketData(DataInput in) throws IOException {
		entityId = in.readInt();
		title = readString(in, "SkullAndRoses".length());
		xPosition = in.readInt();
		yPosition = in.readInt();
		zPosition = in.readInt();
		direction = in.readInt();
	}

	@Override
	public void writePacketData(DataOutput out) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		// TODO Auto-generated method stub

	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 25;
	}

}
