package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.security.PublicKey;





import com.bignigginit.CryptManager;
import com.bignigginit.NetHandler;
import com.bignigginit.Packet;



public class Packet253ServerAuthData extends Packet{
	
	private String serverId;
    private PublicKey publicKey;
    private byte[] verifyToken;

	@Override
	public void readPacketData(DataInput datainput) throws IOException {
		this.serverId = readString(datainput, 20);
        this.publicKey = CryptManager.decodePublicKey(readBytesFromStream(datainput));
        this.verifyToken = readBytesFromStream(datainput);
		
	}

	@Override
	public void writePacketData(DataOutput dataoutput) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		//System.out.println("a" + theNetHandler.connection.getBot().getUsername() + this.verifyToken);
		theNetHandler.handleServerAuthData(this);
		
	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 253;
	}
	

	    public String getServerId()
	    {
	        return this.serverId;
	    }


	    public PublicKey getPublicKey()
	    {
	        return this.publicKey;
	    }
	    public byte[] getVerifyToken()
	    {
	        return this.verifyToken;
	    }

}
