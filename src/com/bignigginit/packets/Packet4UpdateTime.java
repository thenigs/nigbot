package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet4UpdateTime extends Packet{

	/** World age in ticks. */
    public long worldAge;

    /** The world time in minutes. */
    public long time;

    public Packet4UpdateTime() {}

    public Packet4UpdateTime(long par1, long par3, boolean par5)
    {
        this.worldAge = par1;
        this.time = par3;

        if (!par5)
        {
            this.time = -this.time;

            if (this.time == 0L)
            {
                this.time = -1L;
            }
        }
    }

    /**
     * Abstract. Reads the raw packet data from the data stream.
     */
    public void readPacketData(DataInput par1DataInput) throws IOException
    {
        this.worldAge = par1DataInput.readLong();
        this.time = par1DataInput.readLong();
    }

    /**
     * Abstract. Writes the raw packet data to the data stream.
     */
    public void writePacketData(DataOutput par1DataOutput) throws IOException
    {
        par1DataOutput.writeLong(this.worldAge);
        par1DataOutput.writeLong(this.time);
    }

    /**
     * Passes this Packet on to the NetHandler for processing.
     */
    public void processPacket(NetHandler par1NetHandler)
    {
        par1NetHandler.handleUpdateTime(this);
    }

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 4;
	}

}
