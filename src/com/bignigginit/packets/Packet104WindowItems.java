package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.List;



import com.bignigginit.ItemStack;
import com.bignigginit.NetHandler;
import com.bignigginit.Packet;



public class Packet104WindowItems extends Packet{

	/**
     * The id of window which items are being sent for. 0 for player inventory.
     */
    public int windowId;

    /** Stack of items */
    public ItemStack[] itemStack;

    public Packet104WindowItems() {}
/*
    public Packet104WindowItems(int par1, List par2List)
    {
        this.windowId = par1;
        this.itemStack = new ItemStack[par2List.size()];

        for (int j = 0; j < this.itemStack.length; ++j)
        {
            ItemStack itemstack = (ItemStack)par2List.get(j);
            this.itemStack[j] = itemstack == null ? null : itemstack.copy();
        }
    }
*/
    /**
     * Abstract. Reads the raw packet data from the data stream.
     */
    public void readPacketData(DataInput in) throws IOException
    {
    	windowId = in.readByte();
		short count = in.readShort();
		itemStack = new ItemStack[count];

		for(int i = 0; i < count; i++)
			itemStack[i] = readItemStack(in);
    }
    
    

    /**
     * Abstract. Writes the raw packet data to the data stream.
     */
    public void writePacketData(DataOutput par1DataOutput) throws IOException
    {
        
    }

    /**
     * Passes this Packet on to the NetHandler for processing.
     */
    public void processPacket(NetHandler par1NetHandler)
    {
        par1NetHandler.handleWindowItems(this);
    }


	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 104;
	}
	
}
