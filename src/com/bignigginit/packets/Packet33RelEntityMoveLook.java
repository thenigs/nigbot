package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;

public class Packet33RelEntityMoveLook extends Packet30Entity{
	
	public Packet33RelEntityMoveLook()
    {
        this.rotating = true;
    }
	
	public void readPacketData(DataInput par1DataInput) throws IOException
    {
        super.readPacketData(par1DataInput);
        this.xPosition = par1DataInput.readByte();
        this.yPosition = par1DataInput.readByte();
        this.zPosition = par1DataInput.readByte();
        this.yaw = par1DataInput.readByte();
        this.pitch = par1DataInput.readByte();
    }
	
	@Override
	public void writePacketData(DataOutput dataoutput) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		super.processPacket(theNetHandler);
	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 33;
	}

}
