package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet208SetDisplayObjective extends Packet {

	private int int1;
	private String string1;
	
	@Override
	public void readPacketData(DataInput in) throws IOException {
		int1 = in.readByte();
		string1 = readString(in, 16);
	}

	@Override
	public void writePacketData(DataOutput out) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		// TODO Auto-generated method stub

	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 208;
	}

}
