package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet8UpdateHealth extends Packet{

	public float healthMP;
	public int food;

	public float foodSaturation;
	
	@Override
	public void readPacketData(DataInput in) throws IOException {
		healthMP = in.readFloat();
		food = in.readShort();
		foodSaturation = in.readFloat();
	}

	@Override
	public void writePacketData(DataOutput out) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		theNetHandler.handeUpdateHealth(this);
		
	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 8;
	}

}
