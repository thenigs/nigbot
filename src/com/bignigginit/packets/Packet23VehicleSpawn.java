package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet23VehicleSpawn extends Packet{

	public int entityId;

	public int xPosition;
	public int yPosition;
	public int zPosition;
	public int speedX;
	public int speedY;
	public int speedZ;
	public int yaw;
	public int pitch;

	public int type;
	public int throwerEntityId;
	
	@Override
	public void readPacketData(DataInput in) throws IOException {
		entityId = in.readInt();
		type = in.readByte();
		xPosition = in.readInt();
		yPosition = in.readInt();
		zPosition = in.readInt();
		yaw = in.readByte();
		pitch = in.readByte();
		throwerEntityId = in.readInt();

		if(throwerEntityId > 0) {
			speedX = in.readShort();
			speedY = in.readShort();
			speedZ = in.readShort();
		}
	}

	@Override
	public void writePacketData(DataOutput out) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 23;
	}

}
