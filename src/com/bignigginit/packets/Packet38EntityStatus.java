package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet38EntityStatus extends Packet {

	public int entityId;
	public byte entityStatus;
	
	@Override
	public void readPacketData(DataInput in) throws IOException {
		entityId = in.readInt();
		entityStatus = in.readByte();
	}

	@Override
	public void writePacketData(DataOutput out) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		// TODO Auto-generated method stub

	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 38;
	}

}
