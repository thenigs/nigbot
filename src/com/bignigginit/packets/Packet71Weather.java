package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet71Weather extends Packet {

	public int entityID;

	public int posX;
	public int posY;
	public int posZ;

	public int isLightningBolt;
	
	@Override
	public void readPacketData(DataInput in) throws IOException {
		entityID = in.readInt();
		isLightningBolt = in.readByte();
		posX = in.readInt();
		posY = in.readInt();
		posZ = in.readInt();
	}

	@Override
	public void writePacketData(DataOutput out) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		// TODO Auto-generated method stub

	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 71;
	}

}
