package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet207SetScore extends Packet {

	private String string1 = "";
	private String string2 = "";
	private int int1 = 0;
	private int int2 = 0;

	
	@Override
	public void readPacketData(DataInput in) throws IOException {
		string1 = readString(in, 16);
		int2 = in.readByte();

		if(int2 != 1) {
			string2 = readString(in, 16);
			int1 = in.readInt();
		}
	}

	@Override
	public void writePacketData(DataOutput out) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		// TODO Auto-generated method stub

	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 207;
	}

}
