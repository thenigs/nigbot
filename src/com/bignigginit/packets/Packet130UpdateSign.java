package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet130UpdateSign extends Packet{

	public int xPosition;
    public int yPosition;
    public int zPosition;
    public String[] signLines;
    
    
    public Packet130UpdateSign()
    {
        
    }

	
	@Override
	public void readPacketData(DataInput datainput) throws IOException {
		this.xPosition = datainput.readInt();
        this.yPosition = datainput.readShort();
        this.zPosition = datainput.readInt();
        this.signLines = new String[4];

        for (int i = 0; i < 4; ++i)
        {
            this.signLines[i] = readString(datainput, 15);
        }
        
        

        
	}

	@Override
	public void writePacketData(DataOutput dataoutput) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 130;
	}

}
