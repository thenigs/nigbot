package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet28EntityVelocity extends Packet{

	
	public int entityId;
    public int motionX;
    public int motionY;
    public int motionZ;
	
	@Override
	public void readPacketData(DataInput datainput) throws IOException {
		this.entityId = datainput.readInt();
        this.motionX = datainput.readShort();
        this.motionY = datainput.readShort();
        this.motionZ = datainput.readShort();
	}

	@Override
	public void writePacketData(DataOutput dataoutput) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 28;
	}

}
