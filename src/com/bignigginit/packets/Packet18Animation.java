package com.bignigginit.packets;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.bignigginit.NetHandler;
import com.bignigginit.Packet;

public class Packet18Animation extends Packet {

	public static enum Animation {
		NONE(0),
		SWING_ARM(1),
		DAMAGE_ENTITY(2),
		LEAVE_BED(3),
		EAT_FOOD(5),
		UNKNOWN1(102),
		CROUCH(104),
		UNCROUCH(105);

		private final int id;

		private Animation(int id) {
			this.id = id;
		}

		public int getId() {
			return id;
		}

		private static Animation parseAnimation(int id) {
			for(Animation animation : values())
				if(animation.getId() == id)
					return animation;
			return null;
		}
	}
	
	public int entityId;
	public Animation animation;
	
	@Override
	public void readPacketData(DataInput in) throws IOException {
		entityId = in.readInt();
		animation = Animation.parseAnimation(in.readByte());
	}

	@Override
	public void writePacketData(DataOutput dataoutput) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void processPacket(NetHandler theNetHandler) {
		// TODO Auto-generated method stub

	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 18;
	}

}
